CREATE TABLE user (
    user_id int AUTO_INCREMENT PRIMARY KEY,
    email varchar(32) not null unique,
    password varchar(32) not null,
    fullname varchar(50) not null,
    phone_number char(32) not null unique,
    position char(32) default '',
    company_id int default 0,
    address varchar(255) default '',
    role_user int not null,
    picture_user varchar(100) default '',
    gender int not null,
    access_token varchar(32) default '',
    is_deleted int default 0,
    last_update timestamp default current_timestamp on update current_timestamp
);
create index user_active on user (user_id, is_deleted);
create index user_email on user (email, is_deleted);
create index user_password on user (password);
create index user_account_with_email on user (email, password);
create index user_access_token on user (access_token);

create table role (
    role_id int auto_increment primary key,
    role_name char(20) not null,
    is_deleted int default 0,
    create_at timestamp default current_timestamp
);
create index role_name on role (role_name);

create table article (
    article_id int auto_increment primary key,
    user_id int not null,
    title varchar(50) not null unique,
    description varchar(2000) not null,
    picture varchar(100) default '',
    is_deleted int default 0,
    create_at timestamp default current_timestamp,
    update_at timestamp on update current_timestamp
);
create index article_user on article (user_id, is_deleted);
create index article_title on article (title, is_deleted);

create table company (
    company_id int auto_increment primary key,
    name varchar(100) not null,
    address varchar(100) not null,
    phone_number varchar(20) not null,
    is_active int default 0,
    is_deleted int default 0,
    started_at timestamp default current_timestamp,
    update_at timestamp on update current_timestamp
);
create index name_of_company on company (name);

create table content (
    content_id int auto_increment primary key,
    title varchar(100) not null unique,
    description varchar(2000) not null,
    is_deleted int default 0,
    create_at timestamp default current_timestamp,
    update_at timestamp on update current_timestamp
);
create index content_title on content (title);

create table feedback (
    feedback_id int auto_increment primary key,
    user_id int not null,
    comment varchar(1000) not null,
    create_at timestamp default current_timestamp,
    category varchar(15)
);
create index user_feedback on feedback (user_id, feedback_id);

create table device (
    main_device_id int auto_increment primary key,
    device_id varchar(100),
    company_id int not null,
    counter int default 0,
    latitude float default 0.0,
    longitude float default 0.0,
    status_battery int default 0,
    last_calibration timestamp,
    is_active int default 0,
    is_deleted int default 0,
    create_at timestamp default current_timestamp
);
create index company_device on device (company_id);
create index device_device on device (device_id);

create table measurement_device (
    measurement_id int auto_increment primary key,
    device_id varchar(100) not null,
    salt_type int not null,
    total_measurement int default 0
);
create index device_salt_measurement on measurement_device (device_id, salt_type);
create index measure_device on measurement_device (device_id);
create index measure_salt on measurement_device (salt_type);

create table salt_a (
    salt_a_id int auto_increment primary key,
    device_id VARCHAR(255) not null,
    nacl float default 0.0,
    whiteness float default 0.0,
    water_content float default 0.0,
    sample_name varchar(20) not null,
    latitude float default 0.0,
    longitude float default 0.0,
    user_id int not null,
    company_id int not null,
    counter int default 0,
    status_battery int default 0,
    create_at timestamp default current_timestamp
);
create index salt_a_device on salt_a (device_id, salt_a_id);
create index salt_a_name on salt_a (sample_name);
create index salt_a_user on salt_a (user_id);
create index salt_a_company on salt_a (company_id);

create table salt_b (
    salt_b_id int auto_increment primary key,
    device_id varchar(255) not null,
    iodium float default 0.0,
    sample_name varchar(20) not null,
    latitude float default 0.0,
    longitude float default 0.0,
    user_id int not null,
    company_id int not null,
    counter int default 0,
    status_battery int default 0,
    create_at timestamp default current_timestamp
);
create index salt_b_device on salt_b (device_id, salt_b_id);
create index salt_b_name on salt_b (sample_name);
create index salt_b_user on salt_b (user_id);
create index salt_b_company on salt_b (company_id);

create table banner (
    banner_id int auto_increment primary key,
    picture varchar(100) not null,
    is_deleted int default 0,
    is_confirmed int default 0
);