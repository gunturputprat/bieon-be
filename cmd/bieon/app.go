package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"syscall"

	"github.com/gorilla/mux"
	"gitlab.com/bieon-be/common/config"
	"gitlab.com/bieon-be/common/inject"
	"gitlab.com/bieon-be/common/smtp"
	"gitlab.com/bieon-be/handler/rest"
)

func main() {
	flag.Parse()

	// set logger
	f, err := os.OpenFile("cmd/bieon/error.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)

	err = syscall.Dup2(int(f.Fd()), int(os.Stderr.Fd()))
	if err != nil {
		log.Fatalf("Failed to redirect stderr to file: %v", err)
	}

	appConfig := config.InitConfig()
	dbConns := appConfig.GetDatabaseConnection()
	cfg := appConfig.GetPlainConfig()
	routes := rest.InitPackage(cfg)
	smtp.InitSmtp(cfg.CommonConfig.SmtpInfo)
	inject.DependencyInjection(inject.InjectData{
		PlainCfg: cfg,
		DBConns:  dbConns,
		Routes:   routes,
	})
	router := mux.NewRouter()
	routes.InitRoutes(router)
	log.Printf("Service running on port: %v", cfg.CommonConfig.ServerInfo.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprint(":", cfg.CommonConfig.ServerInfo.Port), router))
}
