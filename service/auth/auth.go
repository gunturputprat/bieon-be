package auth

import (
	"context"
	"crypto/md5"
	"database/sql"
	"fmt"
	"log"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"

	"gitlab.com/bieon-be/common/smtp"
	coreAuth "gitlab.com/bieon-be/core/auth"
)

// GetUserInfoByID get user info by user id
func (svc *SvcAuth) GetUserInfoByID(ctx context.Context, userID int64) (userData coreAuth.UserInfo, err error) {
	if userID == 0 {
		err = errors.New("Invalid user id")
		log.Println(err)
		return
	}

	userData, err = svc.CoreAuth.GetUserInfoByID(ctx, userID)
	if err != nil {
		err = errors.Wrap(err, "[GetUserInfoByID] error when get user info")
		log.Println(err)
		return
	}

	if userData.UserID == 0 {
		err = errors.New("[GetUserInfoByID] User not found")
		log.Println(err)
		return
	}

	return userData, nil
}

// Login get user data info by email and password
func (svc *SvcAuth) Login(data coreAuth.LoginInput) (userData coreAuth.UserInfo, err error) {
	data.Password = generatePassword(data.Password)
	userData, err = svc.CoreAuth.GetUserInfoByEmail(data.Email)
	if err != nil {
		err = errors.Wrap(err, "[Service][Login] error when get user info by email")
		log.Println(err)
		return
	}

	if userData.UserID == 0 {
		err = errors.New("[Login] unregistered email")
		log.Println(err)
		return
	}

	if data.Password != userData.Password {
		err = errors.New("[Login] invalid password")
		log.Println(err)
		return
	}

	userData.AccessToken, err = setAuth(userData.UserID, userData.Role, userData.CompanyID, userData.Email)
	if err != nil {
		err = errors.Wrap(err, "[Service][Login] error when set aut")
		log.Println(err)
		return
	}

	return
}

// Register create a new user data
func (svc *SvcAuth) Register(data coreAuth.UserInput) (userData coreAuth.UserInfo, err error) {
	data.Password = generatePassword(data.Password)
	userID, err := svc.CoreAuth.CreateUser(data)
	if err != nil {
		err = errors.Wrap(err, "[Register] failed create new user")
		log.Println(err)
		return
	}

	userData.UserID = userID
	userData.AccessToken, err = setAuth(userData.UserID, userData.Role, userData.CompanyID, userData.Email)
	if err != nil {
		err = errors.Wrap(err, "[Service][Register] error when set auth")
		log.Println(err)
		return
	}
	return
}

func (svc *SvcAuth) UpdateUserData(data coreAuth.UserInput) (err error) {
	if data.UserID <= 0 {
		err = errors.New("[Service][UpdateUserData] Invalid user id")
		log.Println(err)
		return
	}

	err = svc.CoreAuth.UpdateUserData(data)
	if err != nil {
		err = errors.Wrap(err, "[Service][UpdateUserData] failed update user data")
		log.Println(err)
		return
	}

	return nil
}

// ValidateLoginInput for validate login data input
func (svc *SvcAuth) ValidateLoginInput(data coreAuth.LoginInput) (isValid bool, err error) {
	if data.Email == "" {
		err = errors.New("email is empty")
		log.Println(err)
		return false, err
	}

	if data.Password == "" {
		err = errors.New("email is empty")
		log.Println(err)
		return false, err
	}

	return true, nil
}

// ValidateRegisterInput for validate regiter data input
func (svc *SvcAuth) ValidateRegisterInput(data coreAuth.UserInput) (isValid bool, err error) {
	if data.Email == "" {
		err = errors.New("[Service] [ValidateRegisterInput] Invalid email input")
		log.Println(err)
		return false, err
	}

	userData, err := svc.CoreAuth.GetUserInfoByEmail(data.Email)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Service] [ValidateRegisterInput] Error when get user info by email")
		log.Println(err)
		return false, err
	}

	if userData.UserID != 0 {
		err = errors.New("[Service] [ValidateRegisterInput] Email has been registered")
		log.Println(err)
		return false, err
	}

	if data.Password == "" || len(data.Password) < 6 {
		err = errors.New("[Service][ValidateRegisterInput] Invalid password input")
		log.Println(err)
		return false, err
	}

	if data.Name == "" || len(data.Name) < 4 || len(data.Name) > 50 {
		err = errors.New("[Service] [ValidateRegisterInput] Invalid name input")
		log.Println(err)
		return false, err
	}

	if data.PhoneNumber == "" || len(data.PhoneNumber) < 7 || len(data.PhoneNumber) > 12 {
		err = errors.New("[Service] [ValidateRegisterInput] Invalid phone numbaer input")
		log.Println(err)
		return false, err
	}

	if data.CompanyID == 0 {
		err = errors.New("[Service] [ValidateRegisterInput] Invalid company id")
		log.Println(err)
		return false, err
	}

	return true, nil
}

func (svc *SvcAuth) ValidateUpdateInput(data coreAuth.UserInput, oldData coreAuth.UserInfo) (isValid bool, err error) {
	if data.Email != oldData.Email {
		if data.Email == "" {
			err = errors.New("[Service] [ValidateUpdateInput] Invalid new email")
			log.Println(err)
			return false, err
		}

		userData, err := svc.CoreAuth.GetUserInfoByEmail(data.Email)
		if err != nil {
			err = errors.Wrap(err, "[Service] [ValidateUpdateInput] Failed get email data")
			return false, err
		}

		if userData.UserID != 0 {
			return false, errors.New("[Service] [ValidateUpdateInput] Email has been registered")
		}
	}

	if data.Name != oldData.Name {
		if data.Name == "" || len(data.Name) < 4 || len(data.Name) > 50 {
			return false, errors.New("[Service] [ValidateUpdateInput] Invalid name input")
		}
	}

	if data.PhoneNumber != oldData.PhoneNumber {
		if data.PhoneNumber == "" || len(data.PhoneNumber) < 7 || len(data.PhoneNumber) > 12 {
			return false, errors.New("[Service] [ValidateUpdateInput] Invalid phone number input")
		}
	}

	if data.CompanyID != oldData.CompanyID {
		if data.CompanyID == 0 {
			return false, errors.New("[Service] [ValidateUpdateInput] Invalid company id")
		}
	}

	return true, nil
}

func (svc *SvcAuth) UpdatePassword(userID int64, password string) (err error) {
	password = generatePassword(password)
	err = svc.CoreAuth.UpdatePassword(userID, password)
	if err != nil {
		return
	}
	return
}

func (svc *SvcAuth) ValidateUpdatePassword(ctx context.Context, input coreAuth.ChangePasswordInput) (isValid bool, msg string) {
	if input.UserID <= 0 {
		return false, "invalid user id"
	}
	if input.NewPassword != input.RePassword {
		return false, "new password didn't match"
	}
	if input.OldPassword == input.NewPassword {
		return false, "password didn't changed"
	}
	userData, err := svc.CoreAuth.GetUserInfoByID(ctx, input.UserID)
	if err != nil {
		log.Println(err)
		return false, "failed get user data by user id"
	}
	if userData.UserID <= 0 {
		return false, "user data not found"
	}
	if userData.Password != generatePassword(input.OldPassword) {
		return false, "old password didn't match"
	}
	return true, ""
}

func (svc *SvcAuth) DeleteUser(ctx context.Context, userID int64) (err error) {
	if userID <= 0 {
		return errors.New("[Service][DeleteUser] invalid user id")
	}
	userInfo, err := svc.CoreAuth.GetUserInfoByID(ctx, userID)
	if err != nil {
		log.Println(err)
		return
	}
	if userInfo.UserID == 0 {
		return errors.New("[Service][DeleteUser] user not found")
	}
	err = svc.CoreAuth.DeleteUser(userID)
	if err != nil {
		log.Println(err)
		return
	}
	return
}

// generatePassword generate hash password
func generatePassword(password string) (newPassword string) {
	md5 := md5.New()
	md5.Write([]byte(password))
	return fmt.Sprintf("%x", md5.Sum(nil))
}

// generateAccessToken generate access token
func generateAccessToken(email string, password string, withoutTime bool) (accessToken string) {
	combination := fmt.Sprintf("%s%s%v", email, password, time.Now())
	if withoutTime {
		combination = fmt.Sprintf("%s%s", email, password)
	}
	md5 := md5.New()
	md5.Write([]byte(combination))
	return fmt.Sprintf("%x", md5.Sum(nil))
}

func setAuth(userID, role, companyID int64, email string) (tokenString string, err error) {
	tk := &coreAuth.TokenInfo{
		UserID:    userID,
		RoleID:    role,
		Email:     email,
		CompanyID: companyID,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 96).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, err = token.SignedString([]byte("sUp3rs3cr3t"))
	if err != nil {
		return tokenString, errors.Wrap(err, "[Service][setAuth] failed signedString")
	}
	return
}

func (svc *SvcAuth) GetListUser(ctx context.Context, lastUserID int64, perPage int) (result coreAuth.ListUser, err error) {
	if perPage == 0 {
		return result, errors.New("[Service][GetListUser] invalid input")
	}
	result.UserInfos, err = svc.CoreAuth.GetListUser(ctx, lastUserID, perPage+1)
	if err != nil {
		return
	}
	if len(result.UserInfos) < perPage {
		result.IsLastPage = true
		perPage = len(result.UserInfos)
	}
	if perPage > 0 {
		result.UserInfos = result.UserInfos[:perPage]
		lastUserID = result.UserInfos[perPage-1].UserID
	}
	return
}

func (svc *SvcAuth) ForgotPassword(email string) (err error) {
	if email == "" {
		return errors.New("[Service][GetUserInfoByEmail] invalid email")
	}
	userdata, err := svc.CoreAuth.GetUserInfoByEmail(email)
	if err != nil {
		return
	}
	if userdata.UserID == 0 {
		return errors.New("[Service][GetUserInfoByEmail] User not found")
	}
	userdata.AccessToken = generateAccessToken(userdata.Email, userdata.Password, true)
	err = svc.CoreAuth.UpdateUserAccessToken(userdata.UserID, userdata.AccessToken)
	if err != nil {
		return
	}
	message := fmt.Sprintf(`
		<p>Hello,</p>
		<p>Are you really forgot your password?</p>
		<p>You can change your password by click link bellow:</p>
		<p><a href="saltdecbieon.matraindonesia.com/newpassword/%v"><b>saltdecbieon.matraindonesia.com/newpassword/%v</b></a></p>
		<br>
		<p>You can ignore this message if you don't forget your password.</p>
		<p>Thank you,</p>
		<p>Matra Team</p>
	`, userdata.AccessToken, userdata.AccessToken)
	err = smtp.SendMail(userdata.Email, "Reset password token", message)
	if err != nil {
		return
	}
	return
}

func (svc *SvcAuth) ResetPassword(email, password, repassword, token string) (err error) {
	if password != repassword {
		return errors.New("Password not same")
	}
	userData, err := svc.CoreAuth.GetUserInfoByEmail(email)
	if err != nil {
		return
	}
	if userData.UserID == 0 {
		return errors.New("User not found")
	}
	accessToken := generateAccessToken(userData.Email, userData.Password, true)
	if accessToken != token {
		return errors.New("Invalid token")
	}
	userData.Password = generatePassword(password)
	err = svc.CoreAuth.UpdatePassword(userData.UserID, userData.Password)
	if err != nil {
		return
	}
	return
}
