package auth

import (
	"context"
	coreAuth "gitlab.com/bieon-be/core/auth"
)

type (
	ISvcAuth interface {
		GetUserInfoByID(ctx context.Context, userID int64) (userData coreAuth.UserInfo, err error)
		Login(data coreAuth.LoginInput) (userData coreAuth.UserInfo, err error)
		Register(data coreAuth.UserInput) (userData coreAuth.UserInfo, err error)
		UpdateUserData(data coreAuth.UserInput) (err error)
		ValidateLoginInput(data coreAuth.LoginInput) (isValid bool, err error)
		ValidateRegisterInput(data coreAuth.UserInput) (isValid bool, err error)
		ValidateUpdateInput(data coreAuth.UserInput, oldData coreAuth.UserInfo) (isValid bool, err error)
		UpdatePassword(userID int64, password string) (err error)
		ValidateUpdatePassword(ctx context.Context, input coreAuth.ChangePasswordInput) (isValid bool, msg string)
		DeleteUser(ctx context.Context, userID int64) (err error)
		GetListUser(ctx context.Context, lastUserID int64, perPage int) (result coreAuth.ListUser, err error)
		ForgotPassword(email string) (err error)
		ResetPassword(email, password, repassword, token string) (err error)
	}

	SvcAuth struct {
		CoreAuth coreAuth.ICoreAuth `inject:"core_auth"`
	}
)
