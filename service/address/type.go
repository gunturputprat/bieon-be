package address

import (
	coreAddress "gitlab.com/bieon-be/core/address"
)

type (
	ISvcAddress interface {
		GetListProviceData() (provicesData []coreAddress.Provice, err error)
		GetListRegenecyDataByProvinceID(provinceID string) (regencyDatas []coreAddress.Regency, err error)
		GetListDistrictDataByRegencyID(regencyID string) (districtDatas []coreAddress.District, err error)
		GetListVillageDataByDistrict(districtID string) (villageDatas []coreAddress.Village, err error)
	}

	SvcAddress struct {
		CoreAddress coreAddress.ICoreAddress `inject:"core_address"`
	}
)
