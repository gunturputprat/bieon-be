package address

import (
	"github.com/pkg/errors"
	coreAddress "gitlab.com/bieon-be/core/address"
	"log"
)

func (svc *SvcAddress) GetListProviceData() (provicesData []coreAddress.Provice, err error) {
	provicesData, err = svc.CoreAddress.GetListProvicesData()
	if err != nil {
		err = errors.Wrap(err, "[GetListProviceData] get List province data")
		log.Println(err)
		return
	}

	if len(provicesData) == 0 {
		err = errors.New("[GetListProviceData] empty data result")
		log.Println(err)
		return
	}

	return
}

func (svc *SvcAddress) GetListRegenecyDataByProvinceID(provinceID string) (regencyDatas []coreAddress.Regency, err error) {
	if provinceID == "" {
		err = errors.New("[GetListRegenecyDataByProvinceID] invalid province id")
		log.Println(err)
		return
	}

	regencyDatas, err = svc.CoreAddress.GetListRegenciesData(provinceID)
	if err != nil {
		err = errors.Wrapf(err, "[GetListRegenecyDataByProvinceID] failed get regency data by provincy id -> %s", provinceID)
		log.Println(err)
		return
	}

	return
}

func (svc *SvcAddress) GetListDistrictDataByRegencyID(regencyID string) (districtDatas []coreAddress.District, err error) {
	if regencyID == "" {
		err = errors.New("[GetListDistrictDataByRegencyID] Invalid regency id")
		log.Println(err)
		return
	}

	districtDatas, err = svc.CoreAddress.GetListDistrictData(regencyID)
	if err != nil {
		err = errors.Wrapf(err, "[GetListDistrictDataByRegencyID] failed get district data by regency id -> %s", regencyID)
		log.Println(err)
		return
	}

	return
}

func (svc *SvcAddress) GetListVillageDataByDistrict(districtID string) (villageDatas []coreAddress.Village, err error) {
	if districtID == "" {
		err = errors.New("[GetListVillageDataByDistrict] Invalid district id")
		log.Println(err)
		return
	}

	villageDatas, err = svc.CoreAddress.GetListVillageData(districtID)
	if err != nil {
		err = errors.Wrapf(err, "[GetListVillageDataByDistrict] failed get village data by district id -> %s", districtID)
		log.Println(err)
		return
	}

	return
}
