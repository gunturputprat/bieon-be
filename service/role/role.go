package role

import (
	"github.com/pkg/errors"

	coreRole "gitlab.com/bieon-be/core/role"
)

func (svc *SvcRole) AddNewRolel(input coreRole.RoleInput) (err error) {
	if input.RoleName == "" {
		return errors.New("Invlaid role name")
	}
	err = svc.CoreRole.AddNewRole(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcRole) EditRole(input coreRole.RoleInput) (err error) {
	if input.RoleID <= 0 {
		return errors.New("Invalid role id")
	}
	roleInfo, err := svc.CoreRole.GetRoleInfoByID(input.RoleID)
	if err != nil {
		return
	}
	if roleInfo.RoleID <= 0 {
		return errors.New("Role not found")
	}
	err = svc.CoreRole.EditRole(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcRole) GetListRoleInfo() (result coreRole.RoleList, err error) {
	result, err = svc.CoreRole.GetRoleList()
	if err != nil {
		return
	}
	return
}

func (svc *SvcRole) GetRoleInfoByID(roleID int64) (result coreRole.RoleInfo, err error) {
	if roleID <= 0 {
		return result, errors.New("Invalid role id")
	}
	result, err = svc.CoreRole.GetRoleInfoByID(roleID)
	if err != nil {
		return
	}
	if result.RoleID <= 0 {
		return result, errors.New("Role not found")
	}
	return
}
