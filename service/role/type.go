package role

import (
	coreRole "gitlab.com/bieon-be/core/role"
)

type (
	ISvcRole interface {
		AddNewRolel(input coreRole.RoleInput) (err error)
		EditRole(input coreRole.RoleInput) (err error)
		GetListRoleInfo() (result coreRole.RoleList, err error)
		GetRoleInfoByID(roleID int64) (result coreRole.RoleInfo, err error)
	}

	SvcRole struct {
		CoreRole coreRole.ICoreRole `inject:"core_role"`
	}
)
