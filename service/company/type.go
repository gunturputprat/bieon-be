package company

import (
	coreCompany "gitlab.com/bieon-be/core/company"
)

type (
	ISvcCompany interface {
		CreateNewCompany(input coreCompany.CompanyInput) (err error)
		ValidateCreateCompany(input coreCompany.CompanyInput) (isValid bool, msg string)
		UpdateCompany(input coreCompany.CompanyInput) (err error)
		ValidateUpdateCompany(input coreCompany.CompanyInput) (isValid bool, msg string)
		DeleteCompany(companyID int64, isDeleted int) (err error)
		UpdateStatusActiveCompany(companyID int64, isActive int) (err error)
		GetDetailCompanyInfo(companyID int64) (result coreCompany.CompanyInfo, err error)
		GetListCompanyPerPage(lastCompanyID int64, maxPerPage int) (result coreCompany.CompanyListPage, err error)
	}

	SvcCompany struct {
		CoreCompany coreCompany.ICoreCompany `inject:"core_company"`
	}
)
