package company

import (
	coreCompany "gitlab.com/bieon-be/core/company"
)

func (svc *SvcCompany) CreateNewCompany(input coreCompany.CompanyInput) (err error) {
	err = svc.CoreCompany.CreateNewCompany(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcCompany) ValidateCreateCompany(input coreCompany.CompanyInput) (isValid bool, msg string) {
	if input.Name == "" {
		return false, "Invalid company name"
	}
	if input.Address == "" {
		return false, "Invalid company address"
	}
	if input.PhoneNumber == "" {
		return false, "Invalid phone number"
	}
	return true, ""
}

func (svc *SvcCompany) UpdateCompany(input coreCompany.CompanyInput) (err error) {
	err = svc.CoreCompany.UpdateCompany(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcCompany) ValidateUpdateCompany(input coreCompany.CompanyInput) (isValid bool, msg string) {
	if input.CompanyID <= 0 {
		return false, "Invalid company id"
	}
	if input.Name == "" {
		return false, "Invalid company name"
	}
	if input.Address == "" {
		return false, "Invalid company address"
	}
	if input.PhoneNumber == "" {
		return false, "Invalid phone number"
	}
	return true, ""
}

func (svc *SvcCompany) DeleteCompany(companyID int64, isDeleted int) (err error) {
	err = svc.CoreCompany.UpdateDeleteStatusCompany(companyID, isDeleted)
	if err != nil {
		return
	}
	return
}

func (svc *SvcCompany) UpdateStatusActiveCompany(companyID int64, isActive int) (err error) {
	err = svc.CoreCompany.UpdateStatusActiveCompany(companyID, isActive)
	if err != nil {
		return
	}
	return
}

func (svc *SvcCompany) GetDetailCompanyInfo(companyID int64) (result coreCompany.CompanyInfo, err error) {
	result, err = svc.CoreCompany.GetDetailCompany(companyID)
	if err != nil {
		return
	}
	return
}

func (svc *SvcCompany) GetListCompanyPerPage(lastCompanyID int64, maxPerPage int) (result coreCompany.CompanyListPage, err error) {
	result.Companies, err = svc.CoreCompany.GetCompanyListPerPage(lastCompanyID, maxPerPage)
	if err != nil {
		return
	}
	if len(result.Companies) < maxPerPage {
		maxPerPage = len(result.Companies)
		result.IsLastPage = true
	}
	if maxPerPage > 0 {
		result.Companies = result.Companies[:maxPerPage]
		result.LastCompanyID = result.Companies[maxPerPage-1].CompanyID
	}
	return
}
