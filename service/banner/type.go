package banner

import (
	coreBanner "gitlab.com/bieon-be/core/banner"
)

type (
	ISvcBanner interface {
		AddNewBanner(input coreBanner.BannerInput) (err error)
		ValidateAddBanner(input coreBanner.BannerInput) (isValid bool, msg string)
		EditBanner(input coreBanner.BannerInput) (err error)
		ValidateEditBanner(input coreBanner.BannerInput) (isValid bool, msg string)
		DeleteBanner(input coreBanner.BannerInput) (err error)
		ValidateDeleteBanner(input coreBanner.BannerInput) (isValid bool, msg string)
		GetDetailBanner(bannerID int64) (result coreBanner.BannerInfo, err error)
		GetListBanner(startBannerID int64, perPage int) (result coreBanner.BannerList, err error)
		GetBanner() (result coreBanner.BannerList, err error)
	}

	SvcBanner struct {
		CoreBanner coreBanner.ICoreBanner `inject:"core_banner"`
	}
)
