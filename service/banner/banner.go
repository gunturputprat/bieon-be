package banner

import (
	"github.com/pkg/errors"

	coreBanner "gitlab.com/bieon-be/core/banner"
)

func (svc *SvcBanner) AddNewBanner(input coreBanner.BannerInput) (err error) {
	err = svc.CoreBanner.AddNewBanner(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcBanner) ValidateAddBanner(input coreBanner.BannerInput) (isValid bool, msg string) {
	if input.Picutre == "" {
		return false, "Invalid picture"
	}
	return true, ""
}

func (svc *SvcBanner) EditBanner(input coreBanner.BannerInput) (err error) {
	err = svc.CoreBanner.EditBanner(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcBanner) ValidateEditBanner(input coreBanner.BannerInput) (isValid bool, msg string) {
	if input.BannerID <= 0 {
		return false, "Invalid banner id"
	}
	banerInfo, err := svc.CoreBanner.GetDetailBanner(input.BannerID)
	if err != nil {
		return false, "Invalid system"
	}
	if banerInfo.BannerID <= 0 {
		return false, "Banner not found"
	}
	return true, ""
}

func (svc *SvcBanner) DeleteBanner(input coreBanner.BannerInput) (err error) {
	err = svc.CoreBanner.DeleteBanner(input)
	if err != nil {
		return
	}
	return
}

func (svc *SvcBanner) ValidateDeleteBanner(input coreBanner.BannerInput) (isValid bool, msg string) {
	if input.BannerID <= 0 {
		return false, "Invalid banner id"
	}
	bannerInfo, err := svc.CoreBanner.GetDetailBanner(input.BannerID)
	if err != nil {
		return false, "Invalid system"
	}
	if bannerInfo.BannerID <= 0 {
		return false, "Banner not found"
	}
	return true, ""
}

func (svc *SvcBanner) GetDetailBanner(bannerID int64) (result coreBanner.BannerInfo, err error) {
	if bannerID <= 0 {
		return result, errors.New("Invalid banner id")
	}
	result, err = svc.CoreBanner.GetDetailBanner(bannerID)
	if err != nil {
		return
	}
	return
}

func (svc *SvcBanner) GetListBanner(startBannerID int64, perPage int) (result coreBanner.BannerList, err error) {
	if startBannerID < 0 || perPage <= 0 {
		return result, errors.New("Invalid input parameter")
	}
	result.Banners, err = svc.CoreBanner.GetListBanner(startBannerID, perPage+1)
	if err != nil {
		return
	}
	if len(result.Banners) < perPage {
		perPage = len(result.Banners)
		result.IsLastPage = true
	}
	if perPage > 0 {
		result.Banners = result.Banners[:perPage]
		result.LastBannerID = result.Banners[perPage-1].BannerID
	}
	return
}

func (svc *SvcBanner) GetBanner() (result coreBanner.BannerList, err error) {
	result.Banners, err = svc.CoreBanner.GetBanners()
	if err != nil {
		return
	}
	return
}
