package device

import (
	"context"
	"database/sql"
	"github.com/pkg/errors"
	"log"

	coreDevice "gitlab.com/bieon-be/core/device"
)

func (svc *SvcDevice) AddDevice(input coreDevice.DeviceInput) (err error) {
	err = svc.CoreDevice.AddDevice(input)
	if err != nil {
		err = errors.Wrap(err, "[Service] [AddDevice] Error when add device")
		log.Println(err)
		return
	}
	return
}

func (svc *SvcDevice) ValidateAddDevice(ctx context.Context, input coreDevice.DeviceInput) (isVaild bool, msg string) {
	if input.DeviceID == "" {
		return false, "Invalid device id"
	}

	if input.CompanyID <= 0 {
		return false, "Invalid Company id"
	}

	deviceData, err := svc.CoreDevice.GetDeviceInfoByID(ctx, input.DeviceID)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Service] [ValidateAddDevice] failed get device info")
		log.Println(err)
		return false, "failed get device info"
	}

	if deviceData.DeviceID != "" {
		return false, "device already registered"
	}

	return true, ""
}

func (svc *SvcDevice) EditDevice(input coreDevice.DeviceInput) (err error) {
	err = svc.CoreDevice.EditDevice(input)
	if err != nil {
		err = errors.Wrap(err, "[Service] [EditDevice] Error when edit device")
		log.Println(err)
		return
	}
	return
}

func (svc *SvcDevice) ValidateEditDevice(ctx context.Context, input coreDevice.DeviceInput) (isValid bool, msg string) {
	if input.DeviceID == "" {
		return false, "Invalid input device id"
	}
	deviceData, err := svc.CoreDevice.GetDeviceInfoByID(ctx, input.DeviceID)
	if err != nil {
		err = errors.Wrap(err, "[Service] [ValidateEditDevice] Error when get device info by id")
		log.Println(err)
		return false, err.Error()
	}
	if deviceData.MainDeviceID == 0 {
		return false, "Device id not found"
	}
	return true, ""
}

func (svc *SvcDevice) GetListDevice(ctx context.Context, mainDeviceID int64, maxPerPage int) (result coreDevice.ListDeviceInfo, err error) {
	result.Devices, err = svc.CoreDevice.GetListDevice(ctx, mainDeviceID, maxPerPage+1)
	if err != nil {
		return
	}
	if len(result.Devices) < maxPerPage {
		result.IsLastPage = true
		maxPerPage = len(result.Devices)
	}
	if maxPerPage > 0 {
		result.Devices = result.Devices[:maxPerPage]
		result.LastDeviceID = result.Devices[maxPerPage-1].MainDeviceID
	}
	return
}

func (svc *SvcDevice) DeleteDevice(input coreDevice.DeviceInput) (err error) {
	input.IsDeleted = 1
	err = svc.CoreDevice.EditDevice(input)
	if err != nil {
		err = errors.Wrap(err, "[Service] [DeleteDevice] Error when edit device")
		log.Println(err)
		return
	}
	return
}

func (svc *SvcDevice) ValidateDeleteDevice(ctx context.Context, input coreDevice.DeviceInput) (isValid bool, msg string) {
	if input.MainDeviceID == 0 {
		return false, "Invalid main device id"
	}
	if input.DeviceID == "" {
		return false, "Invalid device id"
	}
	deviceData, err := svc.CoreDevice.GetDeviceInfoByID(ctx, input.DeviceID)
	if err != nil {
		err = errors.Wrap(err, "[Service] [ValidateDeleteDevice] Error when get device info by id")
		log.Println(err)
		return false, "Failed get device data"
	}
	if deviceData.IsActive == 1 {
		return false, "Device still active"
	}
	if deviceData.MainDeviceID != input.MainDeviceID {
		return false, "Invalid main device id input"
	}
	return true, ""
}

func (svc *SvcDevice) GetDeviceInfoByID(ctx context.Context, deviceID string) (result coreDevice.DeviceInfo, err error) {
	if deviceID == "" {
		return result, errors.New("Invalid device id")
	}
	result, err = svc.CoreDevice.GetDeviceInfoByID(ctx, deviceID)
	if err != nil {
		err = errors.Wrap(err, "[Service] [GetDeviceInfoByID] Error when get device info by id")
		log.Println(err)
		return
	}
	if result.MainDeviceID == 0 {
		return result, errors.New("Device not found")
	}
	return
}
