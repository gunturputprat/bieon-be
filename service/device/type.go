package device

import (
	"context"
	coreDevice "gitlab.com/bieon-be/core/device"
)

type (
	ISvcDevice interface {
		AddDevice(input coreDevice.DeviceInput) (err error)
		ValidateAddDevice(ctx context.Context, input coreDevice.DeviceInput) (isVaild bool, msg string)
		EditDevice(input coreDevice.DeviceInput) (err error)
		ValidateEditDevice(ctx context.Context, input coreDevice.DeviceInput) (isValid bool, msg string)
		GetListDevice(ctx context.Context, mainDeviceID int64, maxPerPage int) (result coreDevice.ListDeviceInfo, err error)
		DeleteDevice(input coreDevice.DeviceInput) (err error)
		ValidateDeleteDevice(ctx context.Context, input coreDevice.DeviceInput) (isValid bool, msg string)
		GetDeviceInfoByID(ctx context.Context, deviceID string) (result coreDevice.DeviceInfo, err error)
	}

	SvcDevice struct {
		CoreDevice coreDevice.ICoreDevice `inject:"core_device"`
	}
)
