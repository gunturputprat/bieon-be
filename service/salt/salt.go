package salt

import (
	"context"
	"github.com/pkg/errors"
	coreSalt "gitlab.com/bieon-be/core/salt"
)

func (svc *SvcSalt) CreateReportSaltA(input coreSalt.SaltAInput) (err error) {
	err = svc.CoreSalt.CreateReportSaltA(input)
	if err != nil {
		return err
	}
	return
}

func (svc *SvcSalt) CreateReportSaltB(input coreSalt.SaltBInput) (err error) {
	err = svc.CoreSalt.CreateReportSaltB(input)
	if err != nil {
		return err
	}
	return
}

func (svc *SvcSalt) ValidateSaltAInput(input coreSalt.SaltAInput) (isValid bool, msg string) {
	if input.DeviceID == "" {
		return false, "Invalid device id"
	}

	if input.SampleName == "" {
		return false, "Invalid sample name"
	}

	if input.UserID <= 0 {
		return false, "Invalid user id"
	}

	if input.CompanyID <= 0 {
		return false, "Invalid company id"
	}

	return true, ""
}

func (svc *SvcSalt) ValidateSaltBInput(input coreSalt.SaltBInput) (isValid bool, msg string) {
	if input.DeviceID == "" {
		return false, "Invalid device id"
	}

	if input.SampleName == "" {
		return false, "Invalid sample name"
	}

	if input.UserID <= 0 {
		return false, "Invalid user id"
	}

	if input.CompanyID <= 0 {
		return false, "Invalid company id"
	}

	return true, ""
}

func (svc *SvcSalt) GetDetailSaltA(ctx context.Context, saltAID int64) (result coreSalt.SaltAResponse, err error) {
	if saltAID <= 0 {
		return result, errors.Errorf("[Service][GetDetailSaltA] Invalid salt a id")
	}
	result, err = svc.CoreSalt.GetDetailSaltA(ctx, saltAID)
	if err != nil {
		return
	}
	if result.SaltAID != saltAID {
		return result, errors.Errorf("[Service][GetDetailSaltA] salt a data not found")
	}
	return
}

func (svc *SvcSalt) GetDetailSaltB(ctx context.Context, saltBID int64) (result coreSalt.SaltBResponse, err error) {
	if saltBID <= 0 {
		return result, errors.Errorf("[Service][GetDetailSaltB] Invalid salt b id")
	}
	result, err = svc.CoreSalt.GetDetailSaltB(ctx, saltBID)
	if err != nil {
		return
	}
	if result.SaltBID != saltBID {
		return result, errors.Errorf("[Service][GetDetailSaltB] salt b data not found")
	}
	return
}

func (svc *SvcSalt) GetListSaltA(ctx context.Context, startSaltAID int64, maxPerPage int) (result coreSalt.SaltAPerPageResponse, err error) {
	if maxPerPage <= 0 || startSaltAID < 0 {
		return result, errors.New("[Service][GetListSaltA]Invalid max per page data or start salt id")
	}
	result.SaltsA, err = svc.CoreSalt.GetListSaltA(ctx, startSaltAID, maxPerPage+1)
	if err != nil {
		return
	}
	if len(result.SaltsA) < maxPerPage {
		result.IsLastPage = true
		maxPerPage = len(result.SaltsA)
	}
	if maxPerPage > 0 {
		result.SaltsA = result.SaltsA[:maxPerPage]
		result.LastSaltAID = result.SaltsA[maxPerPage-1].SaltAID
	}
	return
}

func (svc *SvcSalt) GetListSaltAWithParameter(ctx context.Context, startSaltAID int64, maxPerPage int, option coreSalt.OptionFilterSaltListQuery) (result coreSalt.SaltAPerPageResponse, err error) {
	if maxPerPage <= 0 || startSaltAID < 0 {
		return result, errors.New("[Service][GetListSaltA]Invalid max per page data or start salt id")
	}
	result.SaltsA, err = svc.CoreSalt.GetListSaltAWithParameter(ctx, startSaltAID, maxPerPage+1, option)
	if err != nil {
		return
	}
	if len(result.SaltsA) < maxPerPage {
		result.IsLastPage = true
		maxPerPage = len(result.SaltsA)
	}
	if maxPerPage > 0 {
		result.SaltsA = result.SaltsA[:maxPerPage]
		result.LastSaltAID = result.SaltsA[maxPerPage-1].SaltAID
	}
	return
}

func (svc *SvcSalt) GetListSaltB(ctx context.Context, startSaltBID int64, maxPerPage int) (result coreSalt.SaltBPerPageResponse, err error) {
	if maxPerPage <= 0 || startSaltBID < 0 {
		return result, errors.New("[Service][GetListSaltB]Invalid max per page data or start salt id")
	}
	result.SaltsB, err = svc.CoreSalt.GetListSaltB(ctx, startSaltBID, maxPerPage+1)
	if err != nil {
		return
	}
	if len(result.SaltsB) < maxPerPage {
		result.IsLastPage = true
		maxPerPage = len(result.SaltsB)
	}
	if maxPerPage > 0 {
		result.SaltsB = result.SaltsB[:maxPerPage]
		result.LastSaltBID = result.SaltsB[maxPerPage-1].SaltBID
	}
	return
}

func (svc *SvcSalt) GetListSaltBWithParameter(ctx context.Context, startSaltBID int64, maxPerPage int, option coreSalt.OptionFilterSaltListQuery) (result coreSalt.SaltBPerPageResponse, err error) {
	if maxPerPage <= 0 || startSaltBID < 0 {
		return result, errors.New("[Service][GetListSaltB]Invalid max per page data or start salt id")
	}
	result.SaltsB, err = svc.CoreSalt.GetListSaltBWithParameter(ctx, startSaltBID, maxPerPage+1, option)
	if err != nil {
		return
	}
	if len(result.SaltsB) < maxPerPage {
		result.IsLastPage = true
		maxPerPage = len(result.SaltsB)
	}
	if maxPerPage > 0 {
		result.SaltsB = result.SaltsB[:maxPerPage]
		result.LastSaltBID = result.SaltsB[maxPerPage-1].SaltBID
	}
	return
}

func (svc *SvcSalt) GetListReportMeasuremntSalt(measurementID int64, perPage int, saltType string) (result coreSalt.MeasurementSaltList, err error) {
	if measurementID <= 0 || perPage <= 0 {
		return result, errors.Wrap(err, "[Service][GetListReportMeasuremntSalt] invlaid input")
	}
	if saltType == "salta" {
		result.MeasurementList, err = svc.CoreSalt.GetReportMeasurementSaltAList(measurementID, perPage+1)
		if err != nil {
			return
		}
	} else if saltType == "saltb" {
		result.MeasurementList, err = svc.CoreSalt.GetReportMeasurementSaltBList(measurementID, perPage+1)
		if err != nil {
			return
		}
	} else if saltType == "all" {
		result.MeasurementList, err = svc.CoreSalt.GetReportMeasurementAllSaltList(measurementID, perPage+1)
		if err != nil {
			return
		}
	}
	if len(result.MeasurementList) < perPage {
		result.IsLastPage = true
		perPage = len(result.MeasurementList)
	}
	if perPage > 0 {
		result.MeasurementList = result.MeasurementList[:perPage]
		result.LastMeasurementID = result.MeasurementList[perPage-1].MeasurementID
	}
	return
}

func (svc *SvcSalt) GetDetailReportMeasurementSalt(measurementID int64) (result coreSalt.MeasurementSaltDetail, err error) {
	if measurementID <= 0 {
		return result, errors.Wrap(err, "[Service][GetDetailReportMeasurmentSalt")
	}
	result, err = svc.CoreSalt.GetDetailReportMeasurementSalt(measurementID)
	if err != nil {
		return
	}
	if result.MeasurementID == 0 {
		return result, errors.Wrap(err, "[Service][GetDtailReportMeasurementSalt] measurement info not found")
	}
	return
}
