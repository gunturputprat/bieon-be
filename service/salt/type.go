package salt

import (
	"context"
	coreSalt "gitlab.com/bieon-be/core/salt"
)

type (
	ISvcSalt interface {
		CreateReportSaltA(input coreSalt.SaltAInput) (err error)
		CreateReportSaltB(input coreSalt.SaltBInput) (err error)
		ValidateSaltAInput(input coreSalt.SaltAInput) (isValid bool, msg string)
		ValidateSaltBInput(input coreSalt.SaltBInput) (isValid bool, msg string)
		GetDetailSaltA(ctx context.Context, saltAID int64) (result coreSalt.SaltAResponse, err error)
		GetDetailSaltB(ctx context.Context, saltBID int64) (result coreSalt.SaltBResponse, err error)
		GetListSaltA(ctx context.Context, startSaltAID int64, maxPerPage int) (result coreSalt.SaltAPerPageResponse, err error)
		GetListSaltAWithParameter(ctx context.Context, startSaltAID int64, maxPerPage int, option coreSalt.OptionFilterSaltListQuery) (result coreSalt.SaltAPerPageResponse, err error)
		GetListSaltB(ctx context.Context, startSaltBID int64, maxPerPage int) (result coreSalt.SaltBPerPageResponse, err error)
		GetListSaltBWithParameter(ctx context.Context, startSaltBID int64, maxPerPage int, option coreSalt.OptionFilterSaltListQuery) (result coreSalt.SaltBPerPageResponse, err error)
		GetListReportMeasuremntSalt(measurementID int64, perPage int, saltType string) (result coreSalt.MeasurementSaltList, err error)
		GetDetailReportMeasurementSalt(measurementID int64) (result coreSalt.MeasurementSaltDetail, err error)
	}

	SvcSalt struct {
		CoreSalt coreSalt.ICoreSalt `inject:"core_salt"`
	}
)
