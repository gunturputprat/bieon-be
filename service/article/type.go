package article

import (
	coreArticle "gitlab.com/bieon-be/core/article"
)

type (
	ISvcArticle interface {
		ArticleAdd(articleInput coreArticle.ArticleInput) (err error)
		ValidateArticleAdd(articleData coreArticle.ArticleInput) (isValid bool, msg string)
		GetArticlePerPage(lastArticleID int64, maxPerPage int) (result coreArticle.ArticlePerPageResponse, err error)
		GetArticleDetail(articleID int64) (result coreArticle.ArticleData, err error)
		ArticleUpdate(articleInput coreArticle.ArticleInput) (err error)
		ValidateArticleUpdate(articleInput coreArticle.ArticleInput) (isValid bool, msg string)
		ArticleDelete(articleInput coreArticle.ArticleInput) (err error)
	}

	SvcArticle struct {
		CoreArticle coreArticle.ICoreArticle `inject:"core_article"`
	}
)
