package article

import (
	"database/sql"
	"github.com/pkg/errors"
	"log"

	coreArticle "gitlab.com/bieon-be/core/article"
)

func (svc *SvcArticle) ArticleAdd(articleInput coreArticle.ArticleInput) (err error) {
	err = svc.CoreArticle.ArticleAdd(articleInput)
	if err != nil {
		err = errors.Wrap(err, "[Service][ArticleAdd] error when add new articel")
		log.Println(err)
		return err
	}
	return nil
}

func (svc *SvcArticle) ValidateArticleAdd(articleInput coreArticle.ArticleInput) (isValid bool, msg string) {
	if articleInput.Title == "" {
		return false, "title cann't be empty"
	}

	if articleInput.Description == "" {
		return false, "description cann't be empty"
	}

	data, err := svc.CoreArticle.GetArticleDetailByTitle(articleInput.Title)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Service][ValidateArticleAdd] error when get article by tittle")
		return false, err.Error()
	}

	if data.ArticleID != 0 && data.UserID == articleInput.UserID {
		return false, "Invalid article input, title cann't be same"
	}

	return true, ""
}

func (svc *SvcArticle) GetArticlePerPage(lastArticleID int64, maxPerPage int) (result coreArticle.ArticlePerPageResponse, err error) {
	result.Articles, err = svc.CoreArticle.GetArticlePerPage(lastArticleID, maxPerPage+1)
	if err != nil {
		err = errors.Wrap(err, "[Service][GetArticlePerPage] error when get data from db")
		log.Println(err)
		return
	}

	if len(result.Articles) < maxPerPage {
		result.IsLastPage = true
		maxPerPage = len(result.Articles)
	}

	if maxPerPage > 0 {
		result.Articles = result.Articles[:maxPerPage]
		lastArticleID = result.Articles[maxPerPage-1].ArticleID
	}

	return
}

func (svc *SvcArticle) GetArticleDetail(articleID int64) (result coreArticle.ArticleData, err error) {
	if articleID <= 0 {
		err = errors.Errorf("[Service][GetArticleDetail] invalid article id, id -> %d", articleID)
		log.Println(err)
		return
	}
	result, err = svc.CoreArticle.GetArticleDetail(articleID)
	if err != nil {
		err = errors.Wrap(err, "[Service][GetArticleDetail] error when get articel from db")
		log.Println(err)
		return
	}

	if result.ArticleID == 0 {
		return result, errors.Errorf("[Service][GetArticleDetail] article not found, article id -> %d", articleID)
	}

	return
}

func (svc *SvcArticle) ArticleUpdate(articleInput coreArticle.ArticleInput) (err error) {
	if articleInput.ArticleID <= 0 {
		err = errors.Errorf("[Service][ArticleUpdate] invalid article id, id -> %d", articleInput.ArticleID)
		log.Println(err)
		return
	}
	err = svc.CoreArticle.ArticleUpdate(articleInput)
	if err != nil {
		err = errors.Wrap(err, "[Service][ArticleUpdate] error when update articel to db")
		log.Println(err)
		return err
	}
	return
}

func (svc *SvcArticle) ValidateArticleUpdate(articleInput coreArticle.ArticleInput) (isValid bool, msg string) {
	if articleInput.ArticleID <= 0 {
		return false, "Invlaid article id"
	}

	if articleInput.Title == "" {
		return false, "Invalid article title, title cann't be empty"
	}

	if articleInput.Description == "" {
		return false, "Invalid article description, description cann't be empty"
	}

	data, err := svc.CoreArticle.GetArticleDetailByTitle(articleInput.Title)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Service][ValidateArticleUpdate] error when get articel by title from db")
		log.Println(err)
		return false, err.Error()
	}

	if data.ArticleID != 0 && articleInput.ArticleID != data.ArticleID && articleInput.UserID == data.UserID {
		return false, "Invalid article title, article title cann't be same"
	}

	data, err = svc.CoreArticle.GetArticleDetail(articleInput.ArticleID)
	if err != nil {
		err = errors.Wrap(err, "[Service][ValidateArticleUpdate] error when get article detail")
		println(err)
		return false, err.Error()
	}

	if data.ArticleID == 0 {
		return false, "invalid article data, article data is not found"
	}

	if data.UserID != articleInput.UserID {
		return false, "Invalid article data, you don't have authorization to edit this article"
	}

	return true, ""
}

func (svc *SvcArticle) ArticleDelete(articleInput coreArticle.ArticleInput) (err error) {
	if articleInput.ArticleID <= 0 {
		err = errors.Errorf("[Service][ArticleDelete] Invalid article id, id -> %d", articleInput.ArticleID)
		log.Println(err)
		return
	}
	articleInput.IsDelete = 1
	err = svc.CoreArticle.ArticleUpdate(articleInput)
	if err != nil {
		err = errors.Wrap(err, "[Service][ArticleDelete] error when update status article")
		log.Println(err)
		return
	}
	return
}
