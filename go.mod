module gitlab.com/bieon-be

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/facebookgo/inject v0.0.0-20180706035515-f23751cae28b
	github.com/facebookgo/structtag v0.0.0-20150214074306-217e25fb9691 // indirect
	github.com/go-sql-driver/mysql v1.4.0
	github.com/gorilla/mux v1.7.4
	github.com/jmoiron/sqlx v1.2.0
	github.com/pkg/errors v0.9.1
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
