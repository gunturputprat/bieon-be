package smtp

import (
	"gitlab.com/bieon-be/common/config"
	"gopkg.in/gomail.v2"
)

var dialer gomail.Dialer
var email string

func InitSmtp(smtpInfo config.SmtpInfo) {
	email = smtpInfo.Email
	dialer = gomail.Dialer{
		Host:     smtpInfo.Host,
		Port:     smtpInfo.Port,
		Username: smtpInfo.Email,
		Password: smtpInfo.Password,
	}
}

func SendMail(receiver, subject, body string) (err error) {
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", email)
	mailer.SetHeader("To", receiver)
	mailer.SetHeader("Subject", subject)
	mailer.SetBody("text/html", body)

	err = dialer.DialAndSend(mailer)
	if err != nil {
		return
	}
	return
}
