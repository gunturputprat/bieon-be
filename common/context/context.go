package context

import (
	"context"
)

type (
	Key string
)

var (
	userKey    = Key("user_id")
	roleKey    = Key("role_id")
	emailKey   = Key("email")
	companyKey = Key("company_id")
)

func SetUserID(ctx context.Context, userID int64) context.Context {
	if ctx == nil {
		ctx = context.Background()
	}
	return context.WithValue(ctx, userKey, userID)
}

func GetUserID(ctx context.Context) (userID int64, ok bool) {
	if ctx == nil {
		return
	}
	if val := ctx.Value(userKey); val != nil {
		userID, ok = val.(int64)
	}
	return
}

func SetRole(ctx context.Context, roleID int64) context.Context {
	if ctx == nil {
		ctx = context.Background()
	}
	return context.WithValue(ctx, roleKey, roleID)
}

func GetRole(ctx context.Context) (roleID int64, ok bool) {
	if ctx == nil {
		return
	}
	if val := ctx.Value(roleKey); val != nil {
		roleID, ok = val.(int64)
	}
	return
}

func SetEmail(ctx context.Context, email string) context.Context {
	if ctx == nil {
		ctx = context.Background()
	}
	return context.WithValue(ctx, emailKey, email)
}

func GetEmail(ctx context.Context) (email string, ok bool) {
	if ctx == nil {
		return
	}
	if val := ctx.Value(emailKey); val != nil {
		email, ok = val.(string)
	}
	return
}

func SetCompanyID(ctx context.Context, companyID int64) context.Context {
	if ctx == nil {
		ctx = context.Background()
	}
	return context.WithValue(ctx, companyKey, companyID)
}

func GetCompany(ctx context.Context) (companyID int64, ok bool) {
	if ctx == nil {
		return
	}
	if val := ctx.Value(companyKey); val != nil {
		companyID, ok = val.(int64)
	}
	return
}
