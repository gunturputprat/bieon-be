package config

import (
	"github.com/jmoiron/sqlx"
)

type (
	AppConfig struct {
		plainConfig *PlainConfig
		dbConns     map[string]*sqlx.DB
	}

	// PlainConfig - hold only config data, used for dependency injection
	PlainConfig struct {
		CommonConfig CommonConfig
		DBConfig     DBConfig
	}

	// CommonConfig contain common configuration
	CommonConfig struct {
		ServerInfo ServerInfo `gcfg:"server"`
		SmtpInfo   SmtpInfo   `gcfg:"smtp"`
		HostInfo   HostInfo
	}

	// ServerInfo contain config for basic server env and configuration
	ServerInfo struct {
		Port string
		Env  string
	}

	SmtpInfo struct {
		Host     string
		Port     int
		Email    string
		Password string
	}

	// HostInfo contain all public url
	HostInfo struct {
		MainUrl string
	}

	// DBConfig contain configuration for db
	DBConfig struct {
		Database map[string]*struct {
			Connection string `gcfg:"connection"`
			MaxConn    string `gcfg:"max-connection"`
			IdleConn   string `gcfg:"idle-connection"`
		}
	}
)
