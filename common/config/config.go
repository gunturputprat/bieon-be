package config

import (
	gcfg "gopkg.in/gcfg.v1"
	"log"
	"os"
	"strconv"

	"github.com/jmoiron/sqlx"

	"gitlab.com/bieon-be/common/database"
)

// InitConfig - readall .ini configuration file and applied to struct
func InitConfig() *AppConfig {
	cfg := CommonConfig{}
	dbCfg := DBConfig{}

	ok := readModuleConfig(&cfg, "configs/etc/bieon", "bieon")
	if !ok {
		log.Fatal("Error read bieon config on configs/etc/bieon/")
	}

	ok = readModuleConfig(&dbCfg, "configs/etc/bieon", "db")
	if !ok {
		log.Fatal("Error read db config on configs/etc/bieon/")
	}

	return &AppConfig{
		plainConfig: &PlainConfig{
			CommonConfig: cfg,
			DBConfig:     dbCfg,
		},
	}
}

// ReadModuleConfig for read file ,ini for set configuration in system
func readModuleConfig(cfg interface{}, path string, module string) bool {
	environ := os.Getenv("TKPENV")
	if environ == "" {
		environ = "development"
	}

	fname := path + "/" + module + "." + environ + ".ini"
	err := gcfg.ReadFileInto(cfg, fname)
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

// GetPlainConfig - return plain struct contains all config
func (g *AppConfig) GetPlainConfig() *PlainConfig {
	if g.plainConfig.CommonConfig.ServerInfo.Port == "" {
		log.Fatal("Config not properly initialized")
	}

	return g.plainConfig
}

func (g *AppConfig) GetDatabaseConnection() map[string]*sqlx.DB {
	if len(g.plainConfig.DBConfig.Database) == 0 {
		log.Fatal("No valid db host exist")
	}

	if g.dbConns != nil {
		return g.dbConns
	}

	dbMap := make(map[string]*sqlx.DB, 0)
	for key, value := range g.plainConfig.DBConfig.Database {
		var maxConn, idleConn int
		var err error
		maxConn, err = strconv.Atoi(value.MaxConn)
		if err != nil || maxConn == 0 {
			maxConn = 10
		}
		idleConn, err = strconv.Atoi(value.IdleConn)
		if err != nil || idleConn == 0 {
			idleConn = 0
		}
		dbConn := database.NewConnection(key, value.Connection, maxConn, idleConn)
		dbMap[key] = dbConn
	}
	g.dbConns = dbMap
	return g.dbConns
}
