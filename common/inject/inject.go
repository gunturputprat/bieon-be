package inject

import (
	"log"

	"github.com/facebookgo/inject"
	"github.com/jmoiron/sqlx"

	"gitlab.com/bieon-be/common/config"
	coreAddress "gitlab.com/bieon-be/core/address"
	coreArticle "gitlab.com/bieon-be/core/article"
	coreAuth "gitlab.com/bieon-be/core/auth"
	coreBanner "gitlab.com/bieon-be/core/banner"
	coreCompany "gitlab.com/bieon-be/core/company"
	coreDevice "gitlab.com/bieon-be/core/device"
	coreRole "gitlab.com/bieon-be/core/role"
	coreSalt "gitlab.com/bieon-be/core/salt"
	"gitlab.com/bieon-be/handler/middleware"
	"gitlab.com/bieon-be/handler/response"
	"gitlab.com/bieon-be/handler/rest"
	svcAddress "gitlab.com/bieon-be/service/address"
	svcArticle "gitlab.com/bieon-be/service/article"
	svcAuth "gitlab.com/bieon-be/service/auth"
	svcBanner "gitlab.com/bieon-be/service/banner"
	svcCompany "gitlab.com/bieon-be/service/company"
	svcDevice "gitlab.com/bieon-be/service/device"
	svcRole "gitlab.com/bieon-be/service/role"
	svcSalt "gitlab.com/bieon-be/service/salt"
)

type (
	InjectData struct {
		PlainCfg *config.PlainConfig
		DBConns  map[string]*sqlx.DB
		Routes   *rest.Routes
	}
)

func DependencyInjection(liq InjectData) {
	dependencies := []*inject.Object{
		&inject.Object{Value: liq.PlainCfg, Name: "config"},
		&inject.Object{Value: liq.DBConns, Name: "db_conns"},

		&inject.Object{Value: &middleware.MiddlewareWrapper{}, Name: "middleware"},
		&inject.Object{Value: &response.RespAPIStandard{}, Name: "handler_response"},

		// core
		&inject.Object{Value: &coreAuth.CoreAuth{}, Name: "core_auth"},
		&inject.Object{Value: &coreAddress.CoreAddress{}, Name: "core_address"},
		&inject.Object{Value: &coreArticle.CoreArticle{}, Name: "core_article"},
		&inject.Object{Value: &coreSalt.CoreSalt{}, Name: "core_salt"},
		&inject.Object{Value: &coreDevice.CoreDevice{}, Name: "core_device"},
		&inject.Object{Value: &coreRole.CoreRole{}, Name: "core_role"},
		&inject.Object{Value: &coreCompany.CoreCompany{}, Name: "core_company"},
		&inject.Object{Value: &coreBanner.CoreBanner{}, Name: "core_banner"},

		// service
		&inject.Object{Value: &svcAuth.SvcAuth{}, Name: "service_auth"},
		&inject.Object{Value: &svcAddress.SvcAddress{}, Name: "service_address"},
		&inject.Object{Value: &svcArticle.SvcArticle{}, Name: "service_article"},
		&inject.Object{Value: &svcSalt.SvcSalt{}, Name: "service_salt"},
		&inject.Object{Value: &svcDevice.SvcDevice{}, Name: "service_device"},
		&inject.Object{Value: &svcRole.SvcRole{}, Name: "service_role"},
		&inject.Object{Value: &svcCompany.SvcCompany{}, Name: "service_company"},
		&inject.Object{Value: &svcBanner.SvcBanner{}, Name: "service_banner"},

		// handler
		&inject.Object{Value: liq.Routes, Name: "routes"},
		&inject.Object{Value: liq.Routes.Auth, Name: "rest_auth"},
		&inject.Object{Value: liq.Routes.Address, Name: "rest_address"},
		&inject.Object{Value: liq.Routes.Article, Name: "rest_article"},
		&inject.Object{Value: liq.Routes.Salt, Name: "rest_salt"},
		&inject.Object{Value: liq.Routes.Device, Name: "rest_device"},
		&inject.Object{Value: liq.Routes.Role, Name: "rest_role"},
		&inject.Object{Value: liq.Routes.Company, Name: "rest_company"},
		&inject.Object{Value: liq.Routes.Images, Name: "rest_images"},
		&inject.Object{Value: liq.Routes.Banner, Name: "rest_banner"},
	}

	var g inject.Graph
	if err := g.Provide(dependencies...); err != nil {
		log.Fatal("Failed Inject Dependencies ", err)
	}

	if err := g.Populate(); err != nil {
		log.Fatal("Failed Populete Inject Dependencies ", err)
	}
}
