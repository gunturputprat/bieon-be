package database

import (
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func NewConnection(dbName string, conn string, maxConn int, idleConn int) *sqlx.DB {
	dbConn, err := sqlx.Connect("mysql", conn)
	if err != nil {
		log.Fatal("Failed to connect master -> ", dbName, ", err ->", err)
	}
	dbConn.SetMaxIdleConns(idleConn)
	dbConn.SetMaxOpenConns(maxConn)
	return dbConn
}
