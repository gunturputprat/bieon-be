package state

const (
	// Role
	RoleOwner    = int64(1)
	RoleClient   = int64(2)
	RoleManager  = int64(3)
	RoleEmployee = int64(4)
)
