package middleware

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcAuth "gitlab.com/bieon-be/service/auth"
)

type (
	//MuxHandler is func type to be parsed to http router handler
	MuxHandler func(http.ResponseWriter, *http.Request)
	//Chain is func type chain before throws to main handler
	Chain func(MuxHandler) MuxHandler
	//Handler is func type to standarize main handler function
	Handler func(http.ResponseWriter, *http.Request)

	IMiddleware interface {
		ChainHandler(endHandler Handler, chains ...Chain) MuxHandler
		InitContext(next MuxHandler) MuxHandler
		VerifyAuth(next MuxHandler) MuxHandler
		SetHeader(next MuxHandler) MuxHandler
		OwnerAccess(next MuxHandler) MuxHandler
		ClientAccess(next MuxHandler) MuxHandler
		ManagerAccess(next MuxHandler) MuxHandler
	}

	MiddlewareWrapper struct {
		Resp    response.IResponse `inject:"handler_response"`
		SvcAuth svcAuth.ISvcAuth   `inject:"service_auth"`
	}
)
