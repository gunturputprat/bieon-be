package middleware

import (
	"context"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"

	commonContext "gitlab.com/bieon-be/common/context"
	commonState "gitlab.com/bieon-be/common/state"
	coreAuth "gitlab.com/bieon-be/core/auth"
)

func (m *MiddlewareWrapper) ChainHandler(endHandler Handler, chains ...Chain) MuxHandler {
	if len(chains) == 0 {
		return handlerWrapper(m, endHandler)
	}

	return chains[0](m.ChainHandler(endHandler, chains[1:]...))
}

var handlerWrapper = func(m *MiddlewareWrapper, h Handler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		h(w, r)
	}
}

func (m *MiddlewareWrapper) InitContext(next MuxHandler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		r = r.WithContext(context.WithValue(r.Context(), "time_start", time.Now()))
		next(w, r)
	}
}

func (m *MiddlewareWrapper) SetHeader(next MuxHandler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET,POST,PATCH,DELETE,OPTIONS")
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, token, Source-Type, Origin")
		next(w, r)
	}
}

func (m *MiddlewareWrapper) VerifyAuth(next MuxHandler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("token")
		if tokenString == "" {
			m.Resp.WriteErrorResponse(w, http.StatusForbidden, []string{"Forbidden access"}, "Forbidden access")
			return
		}
		tk := &coreAuth.TokenInfo{}
		_, err := jwt.ParseWithClaims(tokenString, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte("sUp3rs3cr3t"), nil
		})
		if err != nil {
			m.Resp.WriteErrorResponse(w, http.StatusForbidden, []string{"Forbidden access"}, err.Error())
			return
		}
		ctx := r.Context()
		if tk.UserID == 0 || tk.RoleID == 0 || tk.Email == "" {
			m.Resp.WriteErrorResponse(w, http.StatusForbidden, []string{"Forbidden access"}, "Forbidden access")
			return
		}
		userData, err := m.SvcAuth.GetUserInfoByID(commonContext.SetRole(context.TODO(), commonState.RoleOwner), tk.UserID)
		if err != nil {
			m.Resp.WriteErrorResponse(w, http.StatusForbidden, []string{"Forbidden access"}, err.Error())
			return
		}
		if userData.UserID == 0 || userData.Email != tk.Email || userData.Role != tk.RoleID || userData.CompanyID != tk.CompanyID {
			m.Resp.WriteErrorResponse(w, http.StatusForbidden, []string{"please re-login"}, "Forbidden access")
			return
		}
		ctx = commonContext.SetUserID(ctx, tk.UserID)
		ctx = commonContext.SetRole(ctx, tk.RoleID)
		ctx = commonContext.SetEmail(ctx, tk.Email)
		ctx = commonContext.SetCompanyID(ctx, tk.CompanyID)
		r = r.WithContext(ctx)
		next(w, r)
	}
}

func (m *MiddlewareWrapper) OwnerAccess(next MuxHandler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		role, ok := commonContext.GetRole(ctx)
		if !ok {
			m.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"fobidden access"}, "forbidden access")
			return
		}
		if commonState.RoleOwner < role {
			m.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"forbidden access"}, "forbidden access")
			return
		}
		next(w, r)
	}
}

func (m *MiddlewareWrapper) ClientAccess(next MuxHandler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		role, ok := commonContext.GetRole(ctx)
		if !ok {
			m.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"fobidden access"}, "forbidden access")
			return
		}
		if commonState.RoleClient < role {
			m.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"forbidden access"}, "forbidden access")
			return
		}
		next(w, r)
	}
}

func (m *MiddlewareWrapper) ManagerAccess(next MuxHandler) MuxHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		role, ok := commonContext.GetRole(ctx)
		if !ok {
			m.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"fobidden access"}, "forbidden access")
			return
		}
		if commonState.RoleManager < role {
			m.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"forbidden access"}, "forbidden access")
			return
		}
		next(w, r)
	}
}
