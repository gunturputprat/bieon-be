package response

import (
	"net/http"
	"time"
)

type (
	IResponse interface {
		WriteResponse(w http.ResponseWriter, data interface{}, code int, messages []string, reason string)
		WriteSuccessResponse(w http.ResponseWriter, data interface{})
		WriteErrorResponse(w http.ResponseWriter, code int, messages []string, reason string)
	}

	RespAPIStandard struct {
		Header    Header      `json:"header"`
		Data      interface{} `json:"data"`
		StartTime time.Time   `json:"-"`
	}

	Header struct {
		Messages []string `json:"messages"`
		Reason   string   `json:"reason"`
	}
)
