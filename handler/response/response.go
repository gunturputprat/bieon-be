package response

import (
	"encoding/json"
	"net/http"
)

func (res *RespAPIStandard) WriteSuccessResponse(w http.ResponseWriter, data interface{}) {
	res.WriteResponse(w, data, http.StatusAccepted, []string{}, "")
}

func (res *RespAPIStandard) WriteErrorResponse(w http.ResponseWriter, code int, messages []string, reason string) {
	res.WriteResponse(w, nil, code, messages, reason)
}

func (res *RespAPIStandard) WriteResponse(w http.ResponseWriter, data interface{}, code int, messages []string, reason string) {
	res.Header.Messages = messages
	res.Header.Reason = reason
	res.Data = data
	msg, _ := json.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(msg)
}
