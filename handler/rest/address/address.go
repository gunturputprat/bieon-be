package address

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (h *RestAddress) HandleGetListProvices(w http.ResponseWriter, r *http.Request) {
	proviceData, err := h.SvcAddress.GetListProviceData()
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}

	h.Resp.WriteSuccessResponse(w, proviceData)
	return
}

func (h *RestAddress) HandleGetListRegencyByProvinceID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	provinceID := params["province_id"]
	regencyData, err := h.SvcAddress.GetListRegenecyDataByProvinceID(provinceID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}

	h.Resp.WriteSuccessResponse(w, regencyData)
	return
}

func (h *RestAddress) HandleGetListDistrictByRegencyID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	regencyID := params["regency_id"]
	districtData, err := h.SvcAddress.GetListDistrictDataByRegencyID(regencyID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}

	h.Resp.WriteSuccessResponse(w, districtData)
	return
}

func (h *RestAddress) HandleGetListVillageByDistrictID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	districtID := params["district_id"]
	villageData, err := h.SvcAddress.GetListVillageDataByDistrict(districtID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}

	h.Resp.WriteSuccessResponse(w, villageData)
	return
}
