package address

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcAddress "gitlab.com/bieon-be/service/address"
)

type (
	IRestAddress interface {
		HandleGetListProvices(w http.ResponseWriter, r *http.Request)
		HandleGetListRegencyByProvinceID(w http.ResponseWriter, r *http.Request)
		HandleGetListDistrictByRegencyID(w http.ResponseWriter, r *http.Request)
		HandleGetListVillageByDistrictID(w http.ResponseWriter, r *http.Request)
	}

	RestAddress struct {
		Resp       response.IResponse     `inject:"handler_response"`
		SvcAddress svcAddress.ISvcAddress `inject:"service_address"`
	}
)
