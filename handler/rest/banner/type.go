package banner

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcBanner "gitlab.com/bieon-be/service/banner"
)

type (
	IRestBanner interface {
		HandleAddNewBanner(w http.ResponseWriter, r *http.Request)
		HandleEditBanner(w http.ResponseWriter, r *http.Request)
		HandleDeleteBanner(w http.ResponseWriter, r *http.Request)
		HandleGetDetailBanner(w http.ResponseWriter, r *http.Request)
		HandleGetListBanner(w http.ResponseWriter, r *http.Request)
		HandleGetBanner(w http.ResponseWriter, r *http.Request)
	}

	RestBanner struct {
		SvcBanner svcBanner.ISvcBanner `inject:"service_banner"`
		Resp      response.IResponse   `inject:"handler_response"`
	}
)
