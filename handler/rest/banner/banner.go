package banner

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"

	coreBanner "gitlab.com/bieon-be/core/banner"
)

func (h *RestBanner) HandleAddNewBanner(w http.ResponseWriter, r *http.Request) {
	reqBody := coreBanner.BannerInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
		return
	}
	defer r.Body.Close()
	isValid, msg := h.SvcBanner.ValidateAddBanner(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcBanner.AddNewBanner(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusInternalServerError, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestBanner) HandleEditBanner(w http.ResponseWriter, r *http.Request) {
	reqBody := coreBanner.BannerInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
		return
	}
	defer r.Body.Close()
	isValid, msg := h.SvcBanner.ValidateEditBanner(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcBanner.EditBanner(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusInternalServerError, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestBanner) HandleDeleteBanner(w http.ResponseWriter, r *http.Request) {
	reqBody := coreBanner.BannerInput{}
	params := mux.Vars(r)
	bannerIDStr := params["banner_id"]
	bannerID, _ := strconv.ParseInt(bannerIDStr, 10, 64)
	reqBody.BannerID = bannerID
	isValid, msg := h.SvcBanner.ValidateDeleteBanner(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err := h.SvcBanner.DeleteBanner(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusInternalServerError, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestBanner) HandleGetDetailBanner(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	bannerIDStr := params["banner_id"]
	bannerID, _ := strconv.ParseInt(bannerIDStr, 10, 64)
	if bannerID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid banner id"}, "Invalid banner id")
		return
	}
	data, err := h.SvcBanner.GetDetailBanner(bannerID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusInternalServerError, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestBanner) HandleGetListBanner(w http.ResponseWriter, r *http.Request) {
	var err error
	var startBannerID int64
	var perPage int64 = 10
	startBannerIDStr := r.FormValue("start_banner_id")
	if startBannerIDStr != "" {
		startBannerID, err = strconv.ParseInt(startBannerIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid last article id input"}, err.Error())
			return
		}
	}
	perPageStr := r.FormValue("per_page")
	if perPageStr != "" {
		perPage, err = strconv.ParseInt(perPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid max per page input"}, err.Error())
			return
		}
	}
	data, err := h.SvcBanner.GetListBanner(startBannerID, int(perPage))
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusInternalServerError, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestBanner) HandleGetBanner(w http.ResponseWriter, r *http.Request) {
	data, err := h.SvcBanner.GetBanner()
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusInternalServerError, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}
