package rest

import (
	"gitlab.com/bieon-be/common/config"
	"gitlab.com/bieon-be/handler/middleware"
	restAddress "gitlab.com/bieon-be/handler/rest/address"
	restArticle "gitlab.com/bieon-be/handler/rest/article"
	restAuth "gitlab.com/bieon-be/handler/rest/auth"
	restBanner "gitlab.com/bieon-be/handler/rest/banner"
	restCompany "gitlab.com/bieon-be/handler/rest/company"
	restDevice "gitlab.com/bieon-be/handler/rest/device"
	restImage "gitlab.com/bieon-be/handler/rest/images"
	restRole "gitlab.com/bieon-be/handler/rest/role"
	restSalt "gitlab.com/bieon-be/handler/rest/salt"
)

type (
	Routes struct {
		Config *config.PlainConfig

		Middleware middleware.IMiddleware   `inject:"middleware"`
		Auth       restAuth.IRestAuth       `inject:"rest_auth"`
		Address    restAddress.IRestAddress `inject:"rest_address"`
		Article    restArticle.IRestArticle `inject:"rest_article"`
		Salt       restSalt.IRestSalt       `inject:"rest_salt"`
		Device     restDevice.IRestDevice   `inject:"rest_device"`
		Role       restRole.IRestRole       `inject:"rest_role"`
		Company    restCompany.IRestCompany `inject:"rest_company"`
		Images     restImage.IRestImages    `inject:"rest_images"`
		Banner     restBanner.IRestBanner   `inject:"rest_banner"`
	}
)
