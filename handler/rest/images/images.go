package images

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

func (h *RestImages) HandlerUploadImages(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	target := params["target"]
	err := r.ParseMultipartForm(10 << 20)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	file, handler, err := r.FormFile("image")
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	defer file.Close()
	contentType := handler.Header.Get("Content-type")
	if contentType != "image/png" && contentType != "image/jpeg" {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Must png, jpg, or jpeg"}, "Must png, jpg, or jpeg")
		return
	}
	arrFilename := strings.Split(handler.Filename, ".")
	extension := arrFilename[len(arrFilename)-1]
	fileName := fmt.Sprintf("upload-%s-%v-*.%s", target, time.Now().Unix(), extension)
	tempFile, err := ioutil.TempFile(fmt.Sprintf("public/images/%s", target), fileName)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	defer tempFile.Close()
	fileByte, err := ioutil.ReadAll(file)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	_, err = tempFile.Write(fileByte)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, tempFile.Name())
	return
}
