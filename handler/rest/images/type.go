package images

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
)

type (
	IRestImages interface {
		HandlerUploadImages(w http.ResponseWriter, r *http.Request)
	}

	RestImages struct {
		Resp response.IResponse `inject:"handler_response"`
	}
)
