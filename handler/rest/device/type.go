package device

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcDevice "gitlab.com/bieon-be/service/device"
)

type (
	IRestDevice interface {
		HandleAddDevice(w http.ResponseWriter, r *http.Request)
		HandleEditDevice(w http.ResponseWriter, r *http.Request)
		HandleGetListDevice(w http.ResponseWriter, r *http.Request)
		HandleDeleteDevice(w http.ResponseWriter, r *http.Request)
		HandleDeviceInfo(w http.ResponseWriter, r *http.Request)
	}

	RestDevice struct {
		Resp      response.IResponse   `inject:"handler_response"`
		SvcDevice svcDevice.ISvcDevice `inject:"service_device"`
	}
)
