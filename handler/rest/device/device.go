package device

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"

	coreDevice "gitlab.com/bieon-be/core/device"
)

func (h *RestDevice) HandleAddDevice(w http.ResponseWriter, r *http.Request) {
	reqBody := coreDevice.DeviceInput{}
	ctx := r.Context()
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
		return
	}
	defer r.Body.Close()
	isValid, msg := h.SvcDevice.ValidateAddDevice(ctx, reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcDevice.AddDevice(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestDevice) HandleEditDevice(w http.ResponseWriter, r *http.Request) {
	reqBody := coreDevice.DeviceInput{}
	ctx := r.Context()
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
		return
	}
	defer r.Body.Close()
	isValid, msg := h.SvcDevice.ValidateEditDevice(ctx, reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcDevice.EditDevice(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestDevice) HandleGetListDevice(w http.ResponseWriter, r *http.Request) {
	var mainDeviceID int64 = 999999999999999
	var maxPerPage int64 = 10
	var err error
	ctx := r.Context()
	mainDeviceIDStr := r.FormValue("main_device_id")
	if mainDeviceIDStr != "" {
		mainDeviceID, err = strconv.ParseInt(mainDeviceIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
			return
		}
	}
	maxPerPageStr := r.FormValue("max_per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
			return
		}
	}
	data, err := h.SvcDevice.GetListDevice(ctx, mainDeviceID, int(maxPerPage))
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestDevice) HandleDeleteDevice(w http.ResponseWriter, r *http.Request) {
	reqBody := coreDevice.DeviceInput{}
	ctx := r.Context()
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
		return
	}
	defer r.Body.Close()
	isValid, msg := h.SvcDevice.ValidateDeleteDevice(ctx, reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcDevice.DeleteDevice(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestDevice) HandleDeviceInfo(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	deviceID := params["device_id"]
	ctx := r.Context()
	if deviceID == "" {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid input"}, "Invalid input")
		return
	}
	data, err := h.SvcDevice.GetDeviceInfoByID(ctx, deviceID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}
