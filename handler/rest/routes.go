package rest

import (
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/bieon-be/common/config"
	"gitlab.com/bieon-be/handler/rest/address"
	"gitlab.com/bieon-be/handler/rest/article"
	"gitlab.com/bieon-be/handler/rest/auth"
	"gitlab.com/bieon-be/handler/rest/banner"
	"gitlab.com/bieon-be/handler/rest/company"
	"gitlab.com/bieon-be/handler/rest/device"
	"gitlab.com/bieon-be/handler/rest/images"
	"gitlab.com/bieon-be/handler/rest/role"
	"gitlab.com/bieon-be/handler/rest/salt"
)

func InitPackage(cfg *config.PlainConfig) *Routes {
	return &Routes{
		Config:  cfg,
		Auth:    &auth.RestAuth{},
		Address: &address.RestAddress{},
		Article: &article.RestArticle{},
		Salt:    &salt.RestSalt{},
		Device:  &device.RestDevice{},
		Role:    &role.RestRole{},
		Company: &company.RestCompany{},
		Images:  &images.RestImages{},
		Banner:  &banner.RestBanner{},
	}
}

func (r *Routes) InitRoutes(router *mux.Router) {
	router.HandleFunc("/auth/login", r.Middleware.ChainHandler(r.Auth.HandleAuthLogin, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/auth/login", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/register", r.Middleware.ChainHandler(r.Auth.HandleAuthRegister, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.ClientAccess, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/auth/register", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/update", r.Middleware.ChainHandler(r.Auth.HandleAuthUpdate, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/auth/update", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/change_password", r.Middleware.ChainHandler(r.Auth.HandleChangePassword, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/auth/change_password", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/delete", r.Middleware.ChainHandler(r.Auth.HandleDeleteUser, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.ClientAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/auth/delete", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/list/user", r.Middleware.ChainHandler(r.Auth.HandleUserList, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/auth/list/user", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/user/detail/{user_id}", r.Middleware.ChainHandler(r.Auth.HandlerGetDetailUser, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/auth/user/detail/{user_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/forgot/password", r.Middleware.ChainHandler(r.Auth.HandlerForgotPassword, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/auth/forgot/password", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/auth/reset/password", r.Middleware.ChainHandler(r.Auth.HandleResetPassword, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/auth/reset/password", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/address/list/province", r.Middleware.ChainHandler(r.Address.HandleGetListProvices, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/address/list/province", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/address/list/regency/{province_id}", r.Middleware.ChainHandler(r.Address.HandleGetListRegencyByProvinceID, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/address/list/regency/{province_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/address/list/district/{regency_id}", r.Middleware.ChainHandler(r.Address.HandleGetListDistrictByRegencyID, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/address/list/district/{regency_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/address/list/village/{district_id}", r.Middleware.ChainHandler(r.Address.HandleGetListVillageByDistrictID, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/address/list/village/{district_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/article/list", r.Middleware.ChainHandler(r.Article.HandleGetArticlePerPage, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/article/list", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/article/detail/{article_id}", r.Middleware.ChainHandler(r.Article.HandleGetArticleDetail, r.Middleware.InitContext, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/article/detail/{article_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/article/article-add", r.Middleware.ChainHandler(r.Article.HandleAddArticle, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/article/article-add", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/article/article-update", r.Middleware.ChainHandler(r.Article.HandleArticleUpdate, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/article/article-update", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/article/article-delete", r.Middleware.ChainHandler(r.Article.HandleArticleDelete, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/article/article-delete", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/salt/a/input", r.Middleware.ChainHandler(r.Salt.HandleCreateReportSaltA, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/salt/a/input", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/b/input", r.Middleware.ChainHandler(r.Salt.HandleCreateReportSaltB, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/salt/b/input", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/a/detail/{salt_a_id}", r.Middleware.ChainHandler(r.Salt.HandleGetDetailSaltA, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/salt/a/detail/{salt_a_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/b/detail/{salt_b_id}", r.Middleware.ChainHandler(r.Salt.HandleGetDetailSaltB, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/salt/b/detail/{salt_b_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/a/list", r.Middleware.ChainHandler(r.Salt.HandleGetListSaltA, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/salt/a/list", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/b/list", r.Middleware.ChainHandler(r.Salt.HandleGetListSaltB, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/salt/b/list", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/measurement/list/{salt_type}", r.Middleware.ChainHandler(r.Salt.HandleGetMeasurementSaltList, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.ManagerAccess, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/salt/measurement/list/{salt_type}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/salt/measurement/detail/{measurement_id}", r.Middleware.ChainHandler(r.Salt.HandleGetMeasurementSaltDetail, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.ManagerAccess, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/salt/measurement/detail/{measurement_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/device/device-add", r.Middleware.ChainHandler(r.Device.HandleAddDevice, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/device/device-add", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/device/device-edit", r.Middleware.ChainHandler(r.Device.HandleEditDevice, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/device/device-edit", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/device/list", r.Middleware.ChainHandler(r.Device.HandleGetListDevice, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/device/list", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/device/detail/{device_id}", r.Middleware.ChainHandler(r.Device.HandleDeviceInfo, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/device/detail/{device_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/device/device-delete", r.Middleware.ChainHandler(r.Device.HandleDeleteDevice, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/device/device-delete", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/company/company-add", r.Middleware.ChainHandler(r.Company.HandlerCreateNewCompany, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/company/company-add", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/company/company-edit/{company_id}", r.Middleware.ChainHandler(r.Company.HandlerUpdateCompany, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/company/company-edit/{company_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/company/detail/{company_id}", r.Middleware.ChainHandler(r.Company.HandlerGetDetailCompany, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/company/detail/{company_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/company/company-delete/{company_id}", r.Middleware.ChainHandler(r.Company.HandlerDeleteCompany, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/company/company-delete/{company_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/company/list", r.Middleware.ChainHandler(r.Company.HandlerGetListCompanyPerPage, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/company/list", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/banner/add", r.Middleware.ChainHandler(r.Banner.HandleAddNewBanner, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/banner/add", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/banner/edit", r.Middleware.ChainHandler(r.Banner.HandleEditBanner, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/banner/edit", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/banner/delete/{banner_id}", r.Middleware.ChainHandler(r.Banner.HandleDeleteBanner, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("PATCH")
	router.HandleFunc("/banner/delete/{banner_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/banner/detail/{banner_id}", r.Middleware.ChainHandler(r.Banner.HandleGetDetailBanner, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/banner/detail/{banner_id}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/banner", r.Middleware.ChainHandler(r.Banner.HandleGetBanner, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/banner", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")
	router.HandleFunc("/banner/list", r.Middleware.ChainHandler(r.Banner.HandleGetListBanner, r.Middleware.InitContext, r.Middleware.VerifyAuth, r.Middleware.OwnerAccess, r.Middleware.SetHeader)).Methods("GET")
	router.HandleFunc("/banner/list", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/upload-image/{target}", r.Middleware.ChainHandler(r.Images.HandlerUploadImages, r.Middleware.SetHeader)).Methods("POST")
	router.HandleFunc("/upload-image/{target}", r.Middleware.ChainHandler(optionHandler, r.Middleware.SetHeader)).Methods("OPTIONS")

	router.HandleFunc("/backdoor/auth/register", r.Middleware.ChainHandler(r.Auth.HandleAuthRegister)).Methods("POST")

	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))

	router.HandleFunc("/ping", ping).Methods("GET")

	router.MethodNotAllowedHandler = http.HandlerFunc(notfound)
}

func ping(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Pong"))
}

func notfound(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("404 not found"))
}

func optionHandler(w http.ResponseWriter, r *http.Request) {}
