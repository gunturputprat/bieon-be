package article

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcArticle "gitlab.com/bieon-be/service/article"
)

type (
	IRestArticle interface {
		HandleAddArticle(w http.ResponseWriter, r *http.Request)
		HandleGetArticlePerPage(w http.ResponseWriter, r *http.Request)
		HandleGetArticleDetail(w http.ResponseWriter, r *http.Request)
		HandleArticleUpdate(w http.ResponseWriter, r *http.Request)
		HandleArticleDelete(w http.ResponseWriter, r *http.Request)
	}

	RestArticle struct {
		Resp       response.IResponse     `inject:"handler_response"`
		SvcArticle svcArticle.ISvcArticle `inject:"service_article"`
	}
)
