package article

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	commonContext "gitlab.com/bieon-be/common/context"
	coreArticle "gitlab.com/bieon-be/core/article"
)

func (h *RestArticle) HandleAddArticle(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	reqBody := coreArticle.ArticleInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	defer r.Body.Close()
	reqBody.UserID = userID
	isValid, msg := h.SvcArticle.ValidateArticleAdd(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcArticle.ArticleAdd(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}

	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestArticle) HandleGetArticlePerPage(w http.ResponseWriter, r *http.Request) {
	var err error
	var lastArticleID int64 = 9223372036854775807
	var maxPerPage int64 = 10
	lastArticleIDStr := r.FormValue("last_article_id")
	if lastArticleIDStr != "" {
		lastArticleID, err = strconv.ParseInt(lastArticleIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid last article id input"}, err.Error())
			return
		}
	}
	maxPerPageStr := r.FormValue("per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid max per page input"}, err.Error())
			return
		}
	}
	data, err := h.SvcArticle.GetArticlePerPage(lastArticleID, int(maxPerPage))
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestArticle) HandleGetArticleDetail(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	articleIDStr := params["article_id"]
	articleID, err := strconv.ParseInt(articleIDStr, 10, 64)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid article id input"}, "Invalid article id input")
		return
	}
	if articleID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid article id input"}, "Invalid article id input")
		return
	}
	data, err := h.SvcArticle.GetArticleDetail(articleID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal Server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestArticle) HandleArticleUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid article update input"}, "Invalid article update input")
		return
	}
	reqBody := coreArticle.ArticleInput{}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid article update input"}, err.Error())
		return
	}
	reqBody.UserID = userID
	isValid, msg := h.SvcArticle.ValidateArticleUpdate(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcArticle.ArticleUpdate(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestArticle) HandleArticleDelete(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	reqBody := coreArticle.ArticleInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	data, err := h.SvcArticle.GetArticleDetail(reqBody.ArticleID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	if data.ArticleID != 0 && data.UserID != userID {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Unauthorize to delete this article"}, "Unauthorize to delete this article")
		return
	}
	dataInput := coreArticle.ArticleInput{
		ArticleID:   data.ArticleID,
		Title:       data.Title,
		Description: data.Description,
		UserID:      userID,
	}
	err = h.SvcArticle.ArticleDelete(dataInput)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}
