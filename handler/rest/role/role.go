package role

import (
	"net/http"
)

func (h *RestRole) HandlerGetListRoleInfo(w http.ResponseWriter, r *http.Request) {
	result, err := h.SvcRole.GetListRoleInfo()
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, result)
	return
}
