package role

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcRole "gitlab.com/bieon-be/service/role"
)

type (
	IRestRole interface {
		HandlerGetListRoleInfo(w http.ResponseWriter, r *http.Request)
	}

	RestRole struct {
		Resp    response.IResponse `inject:"handler_response"`
		SvcRole svcRole.ISvcRole   `inject:"service_role"`
	}
)
