package auth

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcAuth "gitlab.com/bieon-be/service/auth"
)

type (
	IRestAuth interface {
		HandleAuthLogin(http.ResponseWriter, *http.Request)
		HandleAuthRegister(w http.ResponseWriter, r *http.Request)
		HandleAuthUpdate(w http.ResponseWriter, r *http.Request)
		HandleChangePassword(w http.ResponseWriter, r *http.Request)
		HandleDeleteUser(w http.ResponseWriter, r *http.Request)
		HandleUserList(w http.ResponseWriter, r *http.Request)
		HandlerGetDetailUser(w http.ResponseWriter, r *http.Request)
		HandlerForgotPassword(w http.ResponseWriter, r *http.Request)
		HandleResetPassword(w http.ResponseWriter, r *http.Request)
	}

	RestAuth struct {
		Resp    response.IResponse `inject:"handler_response"`
		SvcAuth svcAuth.ISvcAuth   `inject:"service_auth"`
	}
)
