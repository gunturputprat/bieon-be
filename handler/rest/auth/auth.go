package auth

import (
	"encoding/json"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	commonContext "gitlab.com/bieon-be/common/context"
	coreAuth "gitlab.com/bieon-be/core/auth"
)

func (h *RestAuth) HandleAuthLogin(w http.ResponseWriter, r *http.Request) {
	reqBody := coreAuth.LoginInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, "Invalid Request")
		return
	}

	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, err.Error())
		return
	}
	defer r.Body.Close()

	isValid, err := h.SvcAuth.ValidateLoginInput(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{err.Error()}, err.Error())
		return
	}

	userData, err := h.SvcAuth.Login(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal Server Error"}, err.Error())
		return
	}

	type temp struct {
		Data  coreAuth.UserInfo `json:"data"`
		Token string            `json:"token"`
	}

	h.Resp.WriteSuccessResponse(w, temp{
		Data:  userData,
		Token: userData.AccessToken,
	})
	return
}

// handler for register a new user
func (h *RestAuth) HandleAuthRegister(w http.ResponseWriter, r *http.Request) {
	reqBody := coreAuth.UserInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, "Invalid Request")
		return
	}

	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, err.Error())
		return
	}
	defer r.Body.Close()

	isValid, err := h.SvcAuth.ValidateRegisterInput(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{err.Error()}, err.Error())
		return
	}

	userData, err := h.SvcAuth.Register(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal Server Error"}, err.Error())
		return
	}

	h.Resp.WriteSuccessResponse(w, userData.AccessToken)
	return
}

func (h *RestAuth) HandleAuthUpdate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	reqBody := coreAuth.UserInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, "Invalid Request")
		return
	}

	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, err.Error())
		return
	}
	defer r.Body.Close()

	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"forbidden access"}, "forbidden access")
		return
	}

	reqBody.UserID = userID
	if reqBody.UserID == 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"forbidden access"}, "forbidden access")
		return
	}

	oldUserData, err := h.SvcAuth.GetUserInfoByID(ctx, reqBody.UserID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusNotFound, []string{"User not found"}, err.Error())
		return
	}

	isValid, err := h.SvcAuth.ValidateUpdateInput(reqBody, oldUserData)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{err.Error()}, err.Error())
		return
	}

	err = h.SvcAuth.UpdateUserData(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Internal Server Error"}, err.Error())
		return
	}

	newData, err := h.SvcAuth.GetUserInfoByID(ctx, reqBody.UserID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusNotFound, []string{"User not found"}, err.Error())
		return
	}

	if oldUserData.Picture != newData.Picture {
		os.Remove(oldUserData.Picture)
	}

	h.Resp.WriteSuccessResponse(w, newData)
	return
}

func (h *RestAuth) HandleChangePassword(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	reqBody := coreAuth.ChangePasswordInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, err.Error())
		return
	}
	defer r.Body.Close()
	reqBody.UserID = userID
	isValid, msg := h.SvcAuth.ValidateUpdatePassword(ctx, reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcAuth.UpdatePassword(reqBody.UserID, reqBody.NewPassword)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestAuth) HandleDeleteUser(w http.ResponseWriter, r *http.Request) {
	reqBody := coreAuth.UserInfo{}
	ctx := r.Context()
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	defer r.Body.Close()
	err = h.SvcAuth.DeleteUser(ctx, reqBody.UserID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestAuth) HandleUserList(w http.ResponseWriter, r *http.Request) {
	var err error
	var lastUserID int64
	var maxPerPage int64 = 10
	ctx := r.Context()
	lastUserIDStr := r.FormValue("last_user_id")
	if lastUserIDStr != "" {
		lastUserID, err = strconv.ParseInt(lastUserIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid last user id input"}, err.Error())
			return
		}
	}
	maxPerPageStr := r.FormValue("per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid max per page input"}, err.Error())
			return
		}
	}
	data, err := h.SvcAuth.GetListUser(ctx, lastUserID, int(maxPerPage))
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestAuth) HandlerGetDetailUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	ctx := r.Context()
	userID, _ := strconv.ParseInt(params["user_id"], 10, 64)
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid user id"}, "Invalid user id")
		return
	}
	data, err := h.SvcAuth.GetUserInfoByID(ctx, userID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestAuth) HandlerForgotPassword(w http.ResponseWriter, r *http.Request) {
	reqBody := coreAuth.UserInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, "Invalid Request")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, err.Error())
		return
	}
	defer r.Body.Close()

	err = h.SvcAuth.ForgotPassword(reqBody.Email)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid request"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestAuth) HandleResetPassword(w http.ResponseWriter, r *http.Request) {
	reqBody := coreAuth.ChangePasswordInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, "Invalid Request")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid Request"}, err.Error())
		return
	}
	defer r.Body.Close()
	err = h.SvcAuth.ResetPassword(reqBody.Email, reqBody.NewPassword, reqBody.RePassword, reqBody.Token)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid request"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}
