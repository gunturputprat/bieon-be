package company

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"

	coreCompany "gitlab.com/bieon-be/core/company"
)

func (h *RestCompany) HandlerCreateNewCompany(w http.ResponseWriter, r *http.Request) {
	reqBody := coreCompany.CompanyInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid input"}, err.Error())
		return
	}
	isValid, msg := h.SvcCompany.ValidateCreateCompany(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{msg}, msg)
		return
	}
	err = h.SvcCompany.CreateNewCompany(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestCompany) HandlerUpdateCompany(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	companyIDStr := params["company_id"]
	companyID, _ := strconv.ParseInt(companyIDStr, 10, 64)
	if companyID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid Input"}, "Invalid input")
		return
	}
	reqBody := coreCompany.CompanyInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid input"}, "Invalid input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid input"}, err.Error())
		return
	}
	reqBody.CompanyID = companyID
	isValid, msg := h.SvcCompany.ValidateUpdateCompany(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{msg}, msg)
		return
	}
	err = h.SvcCompany.UpdateCompany(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestCompany) HandlerGetListCompanyPerPage(w http.ResponseWriter, r *http.Request) {
	var err error
	var lastCompanyID int64
	var maxPerPage int64 = 10
	lastCompanyIDStr := r.FormValue("last_company_id")
	if lastCompanyIDStr != "" {
		lastCompanyID, err = strconv.ParseInt(lastCompanyIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid last company id input"}, err.Error())
			return
		}
	}
	maxPerPageStr := r.FormValue("per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid max per page input"}, err.Error())
			return
		}
	}
	data, err := h.SvcCompany.GetListCompanyPerPage(lastCompanyID, int(maxPerPage))
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestCompany) HandlerDeleteCompany(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	companyIDstr := params["company_id"]
	companyID, _ := strconv.ParseInt(companyIDstr, 10, 64)
	if companyID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Invalid comppany id"}, "Invalid company id")
		return
	}
	err := h.SvcCompany.DeleteCompany(companyID, 1)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestCompany) HandlerGetDetailCompany(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	companyID, _ := strconv.ParseInt(params["company_id"], 10, 64)
	if companyID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid company ID"}, "Invalid company id")
		return
	}
	data, err := h.SvcCompany.GetDetailCompanyInfo(companyID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}
