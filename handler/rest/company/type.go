package company

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcCompany "gitlab.com/bieon-be/service/company"
)

type (
	IRestCompany interface {
		HandlerCreateNewCompany(w http.ResponseWriter, r *http.Request)
		HandlerUpdateCompany(w http.ResponseWriter, r *http.Request)
		HandlerGetListCompanyPerPage(w http.ResponseWriter, r *http.Request)
		HandlerDeleteCompany(w http.ResponseWriter, r *http.Request)
		HandlerGetDetailCompany(w http.ResponseWriter, r *http.Request)
	}

	RestCompany struct {
		Resp       response.IResponse     `inject:"handler_response"`
		SvcCompany svcCompany.ISvcCompany `inject:"service_company"`
	}
)
