package salt

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	commonContext "gitlab.com/bieon-be/common/context"
	coreSalt "gitlab.com/bieon-be/core/salt"
)

func (h *RestSalt) HandleCreateReportSaltA(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	reqBody := coreSalt.SaltAInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid salt a input"}, "Invalid salt a input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid salt a input"}, "Invalid salt a input")
		return
	}
	defer r.Body.Close()
	reqBody.UserID = userID
	isValid, msg := h.SvcSalt.ValidateSaltAInput(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcSalt.CreateReportSaltA(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestSalt) HandleCreateReportSaltB(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	userID, ok := commonContext.GetUserID(ctx)
	if !ok {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	if userID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusUnauthorized, []string{"Forbidden access"}, "forbidden access")
		return
	}
	reqBody := coreSalt.SaltBInput{}
	if r.Body == nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid salt a input"}, "Invalid salt a input")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid salt a input"}, "Invalid salt a input")
		return
	}
	defer r.Body.Close()
	reqBody.UserID = userID
	isValid, msg := h.SvcSalt.ValidateSaltBInput(reqBody)
	if !isValid {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{msg}, msg)
		return
	}
	err = h.SvcSalt.CreateReportSaltB(reqBody)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, nil)
	return
}

func (h *RestSalt) HandleGetDetailSaltA(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	ctx := r.Context()
	saltAIDStr := params["salt_a_id"]
	saltAID, err := strconv.ParseInt(saltAIDStr, 10, 64)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input salt a id"}, "Invalid input salt a id")
		return
	}
	result, err := h.SvcSalt.GetDetailSaltA(ctx, saltAID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, result)
	return
}

func (h *RestSalt) HandleGetDetailSaltB(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	ctx := r.Context()
	saltBIDStr := params["salt_b_id"]
	saltBID, err := strconv.ParseInt(saltBIDStr, 10, 64)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input salt b id"}, "Invalid input salt b id")
		return
	}
	result, err := h.SvcSalt.GetDetailSaltB(ctx, saltBID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, result)
	return
}

func (h *RestSalt) HandleGetListSaltA(w http.ResponseWriter, r *http.Request) {
	var startSaltAID int64
	var maxPerPage int64 = 10
	var userID int64
	var deviceID string
	var err error
	ctx := r.Context()
	startSaltAIDStr := r.FormValue("start_salt_a_id")
	if startSaltAIDStr != "" {
		startSaltAID, err = strconv.ParseInt(startSaltAIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"invalid start salt a id"}, "invalid start salt a id")
			return
		}
	}
	maxPerPageStr := r.FormValue("max_per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"invalid max per page"}, "invalid max per page")
			return
		}
	}
	userIDStr := r.FormValue("user_id")
	if userIDStr != "" {
		userID, _ = strconv.ParseInt(userIDStr, 10, 64)
	}
	deviceID = r.FormValue("device_id")
	option := coreSalt.OptionFilterSaltListQuery{
		UserID:   userID,
		DeviceID: deviceID,
	}
	data, err := h.SvcSalt.GetListSaltAWithParameter(ctx, startSaltAID, int(maxPerPage), option)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestSalt) HandleGetListSaltB(w http.ResponseWriter, r *http.Request) {
	var startSaltBID int64
	var maxPerPage int64 = 10
	var userID int64
	var deviceID string
	var err error
	ctx := r.Context()
	startSaltBIDStr := r.FormValue("start_salt_b_id")
	if startSaltBIDStr != "" {
		startSaltBID, err = strconv.ParseInt(startSaltBIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"invalid start salt b id"}, "invalid start salt b id")
			return
		}
	}
	maxPerPageStr := r.FormValue("max_per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"invalid max per page"}, "invalid max per page")
			return
		}
	}
	userIDStr := r.FormValue("user_id")
	if userIDStr != "" {
		userID, _ = strconv.ParseInt(userIDStr, 10, 64)
	}
	deviceID = r.FormValue("device_id")
	option := coreSalt.OptionFilterSaltListQuery{
		UserID:   userID,
		DeviceID: deviceID,
	}
	data, err := h.SvcSalt.GetListSaltBWithParameter(ctx, startSaltBID, int(maxPerPage), option)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestSalt) HandleGetMeasurementSaltList(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	saltType := params["salt_type"]
	if saltType == "" {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid input"}, "Invalid input")
		return
	}
	var startMeasurementBID int64
	var maxPerPage int64 = 10
	var err error
	startMeasurementBIDStr := r.FormValue("start_measurement_id")
	if startMeasurementBIDStr != "" {
		startMeasurementBID, err = strconv.ParseInt(startMeasurementBIDStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"invalid start salt b id"}, "invalid start salt b id")
			return
		}
	}
	maxPerPageStr := r.FormValue("max_per_page")
	if maxPerPageStr != "" {
		maxPerPage, err = strconv.ParseInt(maxPerPageStr, 10, 64)
		if err != nil {
			h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"invalid max per page"}, "invalid max per page")
			return
		}
	}
	data, err := h.SvcSalt.GetListReportMeasuremntSalt(startMeasurementBID, int(maxPerPage), saltType)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}

func (h *RestSalt) HandleGetMeasurementSaltDetail(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	measurementID, _ := strconv.ParseInt(params["measurement_id"], 10, 64)
	if measurementID <= 0 {
		h.Resp.WriteErrorResponse(w, http.StatusBadRequest, []string{"Invalid request"}, "Invalid request")
		return
	}
	data, err := h.SvcSalt.GetDetailReportMeasurementSalt(measurementID)
	if err != nil {
		h.Resp.WriteErrorResponse(w, http.StatusUnprocessableEntity, []string{"Internal server error"}, err.Error())
		return
	}
	h.Resp.WriteSuccessResponse(w, data)
	return
}
