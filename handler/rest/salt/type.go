package salt

import (
	"net/http"

	"gitlab.com/bieon-be/handler/response"
	svcSalt "gitlab.com/bieon-be/service/salt"
)

type (
	IRestSalt interface {
		HandleCreateReportSaltA(w http.ResponseWriter, r *http.Request)
		HandleCreateReportSaltB(w http.ResponseWriter, r *http.Request)
		HandleGetDetailSaltA(w http.ResponseWriter, r *http.Request)
		HandleGetDetailSaltB(w http.ResponseWriter, r *http.Request)
		HandleGetListSaltA(w http.ResponseWriter, r *http.Request)
		HandleGetListSaltB(w http.ResponseWriter, r *http.Request)
		HandleGetMeasurementSaltList(w http.ResponseWriter, r *http.Request)
		HandleGetMeasurementSaltDetail(w http.ResponseWriter, r *http.Request)
	}

	RestSalt struct {
		Resp    response.IResponse `inject:"handler_response"`
		SvcSalt svcSalt.ISvcSalt   `inject:"service_salt"`
	}
)
