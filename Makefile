#Go params
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGENERATE=$(GOCMD) generate
DEPCMD=dep ensure

CMDDIR=cmd
BIN_NAME=bieon

all: gen build-main run
dep: 
	$(DEPCMD) -v --vendor-only
gen:
	$(GOGENERATE) ./...
build-main:
	rm -f $(CMDDIR)/$(BIN_NAME)/$(BIN_NAME)
	$(GOBUILD) -o $(CMDDIR)/$(BIN_NAME)/$(BIN_NAME) $(CMDDIR)/${BIN_NAME}/app.go
run:
	./$(CMDDIR)/$(BIN_NAME)/$(BIN_NAME)
test:
	$(GOTEST) -v -cover -race ./...
default: all
