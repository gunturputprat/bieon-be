package article

import (
	"time"

	"github.com/jmoiron/sqlx"
)

type (
	ICoreArticle interface {
		GetArticlePerPage(lastArticleID int64, maxPerPage int) (articleData []ArticleData, err error)
		ArticleAdd(articleInput ArticleInput) (err error)
		GetArticleDetail(articleID int64) (articleData ArticleData, err error)
		ArticleUpdate(articleInput ArticleInput) (err error)
		GetArticleDetailByTitle(articleTitle string) (articleData ArticleData, err error)
	}

	CoreArticle struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	ArticleInput struct {
		ArticleID   int64     `db:"article_id" json:"article_id"`
		UserID      int64     `db:"user_id" json:"user_id"`
		Title       string    `db:"title" json:"title"`
		Description string    `db:"description" json:"description"`
		Picture     string    `db:"picture" json:"picture"`
		IsDelete    int       `db:"is_delete" json:"is_deleted"`
		CreateAt    time.Time `db:"create_at" json:"create_at"`
		UpdateAt    time.Time `db:"update_at,omitempty" json:"update_at"`
	}

	ArticleData struct {
		ArticleID   int64     `db:"article_id" json:"article_id"`
		UserID      int64     `db:"user_id" json:"user_id"`
		Title       string    `db:"title" json:"title"`
		Description string    `db:"description" json:"description"`
		Picture     string    `db:"picture" json:"picture"`
		IsDelete    int       `db:"is_delete" json:"is_deleted"`
		CreateAt    time.Time `db:"create_at" json:"create_at"`
		UpdateAt    time.Time `db:"update_at,omitempty" json:"update_at"`
	}

	ArticlePerPageResponse struct {
		Articles      []ArticleData `json:"articles"`
		NextArticleID int64         `json:"next_article_id"`
		IsLastPage    bool          `json:"is_last_page"`
	}
)
