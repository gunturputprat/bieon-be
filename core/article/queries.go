package article

const articleAdd = `
INSERT INTO
	article(title, description, user_id, picture)
VALUES
	(?, ?, ?, ?);
`

const articleUpdate = `
UPDATE
	article
SET
	title = ?,
	description = ?,
	picture = ?,
	is_deleted = ?
WHERE
	article_id = ?;
`

const getArticlePerPage = `
SELECT
	article_id,
	user_id,
	title,
	description,
	picture,
	create_at,
	COALESCE(update_at, create_at) as update_at
FROM
	article
WHERE
	article_id < ? AND
	is_deleted = 0
ORDER BY
	article_id DESC
LIMIT ?;
`

const getArticleDetail = `
SELECT
	article_id,
	user_id,
	title,
	description,
	picture,
	create_at,
	COALESCE(update_at, create_at) as update_at
FROM
	article
WHERE
	article_id = ? AND
	is_deleted = 0;
`

const getArticleDetailByTitle = `
SELECT
	article_id,
	user_id,
	title,
	description,
	picture,
	create_at,
	COALESCE(update_at, create_at) as update_at
FROM
	article
WHERE
	title = ? AND
	is_deleted = 0;
`
