package article

import (
	"database/sql"
	"github.com/pkg/errors"
	"log"
)

func (core *CoreArticle) ArticleAdd(articleInput ArticleInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][ArticleAdd] failed begin tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		articleAdd,
		articleInput.Title,
		articleInput.Description,
		articleInput.UserID,
		articleInput.Picture,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][ArticleAdd] failed insert article data")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][ArticleAdd] failed commit tx")
		log.Println(err)
		return
	}

	return nil
}

func (core *CoreArticle) GetArticlePerPage(lastArticleID int64, maxPerPage int) (articleData []ArticleData, err error) {
	err = core.DBConns["main"].Select(
		&articleData,
		getArticlePerPage,
		lastArticleID,
		maxPerPage,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetAllArticle] failed get data article")
		log.Println(err)
		return articleData, err
	}

	return
}

func (core *CoreArticle) GetArticleDetail(articleID int64) (articleData ArticleData, err error) {
	err = core.DBConns["main"].Get(
		&articleData,
		getArticleDetail,
		articleID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetArticleDetail] failed get article data detail")
		log.Println(err)
		return
	}

	return
}

func (core *CoreArticle) ArticleUpdate(articleInput ArticleInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][ArticleUpdate] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		articleUpdate,
		articleInput.Title,
		articleInput.Description,
		articleInput.Picture,
		articleInput.IsDelete,
		articleInput.ArticleID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][ArticleUpdate] failed update data article")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][ArticleUpdate] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreArticle) GetArticleDetailByTitle(articleTitle string) (articleData ArticleData, err error) {
	err = core.DBConns["main"].Get(
		&articleData,
		getArticleDetailByTitle,
		articleTitle,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetArticleDetailByTitle] failed get article detail by title")
		log.Println(err)
		return
	}
	return
}
