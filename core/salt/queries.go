package salt

const createSaltA = `
INSERT INTO
	salt_a(
		device_id,
		nacl,
		whiteness,
		water_content,
		sample_name,
		latitude,
		longitude,
		user_id,
		company_id,
		counter,
		status_battery
	)
VALUES
	(?,?,?,?,?,?,?,?,?,?,?);
`

const createSaltB = `
INSERT INTO
	salt_b(
		device_id,
		iodium,
		sample_name,
		latitude,
		longitude,
		user_id,
		company_id,
		counter,
		status_battery
	)
VALUES
	(?,?,?,?,?,?,?,?,?);
`

const getDetailSaltA = `
SELECT
	salt_a_id,
	device_id,
	nacl,
	whiteness,
	water_content,
	sample_name,
	latitude,
	longitude,
	user_id,
	company_id,
	counter,
	status_battery,
	create_at
FROM
	salt_a
WHERE
	%v
	salt_a_id = ?;
`

const getDetailSaltB = `
SELECT
	salt_b_id,
	device_id,
	iodium,
	sample_name,
	latitude,
	longitude,
	user_id,
	company_id,
	counter,
	status_battery,
	create_at
FROM
	salt_b
WHERE
	%v
	salt_b_id = ?;
`

const getListSaltA = `
SELECT
	salt_a_id,
	device_id,
	nacl,
	whiteness,
	water_content,
	sample_name,
	latitude,
	longitude,
	user_id,
	company_id,
	counter,
	status_battery,
	create_at
FROM
	salt_a
WHERE
	%v
	salt_a_id > ?
ORDER BY
	salt_a_id ASC
LIMIT ?;
`

const getListSaltAWithOption = `
SELECT
	salt_a_id,
	device_id,
	nacl,
	whiteness,
	water_content,
	sample_name,
	latitude,
	longitude,
	user_id,
	company_id,
	counter,
	status_battery,
	create_at
FROM
	salt_a
WHERE
	salt_a_id > ?
	%v
ORDER BY
	salt_a_id ASC
LIMIT ?;
`

const getListSaltB = `
SELECT
	salt_b_id,
	device_id,
	iodium,
	sample_name,
	latitude,
	longitude,
	user_id,
	company_id,
	counter,
	status_battery,
	create_at
FROM
	salt_b
WHERE
	%v
	salt_b_id > ?
ORDER BY
	salt_b_id ASC
LIMIT ?;
`

const getListSaltBWithOption = `
SELECT
	salt_b_id,
	device_id,
	iodium,
	sample_name,
	latitude,
	longitude,
	user_id,
	company_id,
	counter,
	status_battery,
	create_at
FROM
	salt_b
WHERE
	salt_b_id > ?
	%v
ORDER BY
	salt_b_id ASC
LIMIT ?;
`

const addCounterReport = `
UPDATE
	measurement_device
SET
	total_measurement = ?
WHERE
	device_id = ? AND
	salt_type = ?;
`

const addCounterDevice = `
UPDATE
	device
SET
	counter = counter + 1
WHERE
	device_id = ?;
`

const measurementSaltAList = `
SELECT
	measurement_id,
	device_id,
	salt_type,
	total_measurement
WHERE
	salt_type = 1 AND
	measurement_id > ?
LIMIT ?
`

const measurementSaltBList = `
SELECT
	measurement_id,
	device_id,
	salt_type,
	total_measurement
WHERE
	salt_type = 2 AND
	measurement_id > ?
LIMIT ?
`

const measurementAllSaltList = `
SELECT
	measurement_id,
	device_id,
	salt_type,
	total_measurement
FROM
	measurement_device
WHERE
	measurement_id > ?
LIMIT ?
`

const measurementSaltDetail = `
SELECT
	measurement_id,
	device_id,
	salt_type,
	total_measurement
FROM
	measurement_device
WHERE
	measurement_id = ?
`
