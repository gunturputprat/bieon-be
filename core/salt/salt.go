package salt

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/bieon-be/common/state"
	"log"

	"github.com/pkg/errors"
	commonContext "gitlab.com/bieon-be/common/context"
)

func (core *CoreSalt) CreateReportSaltA(input SaltAInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateReportSaltA] failed when init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		createSaltA,
		input.DeviceID,
		input.Nacl,
		input.Whiteness,
		input.WaterContent,
		input.SampleName,
		input.Lat,
		input.Long,
		input.UserID,
		input.CompanyID,
		input.Counter,
		input.StatusBattery,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateReportSaltA] failed when create report salt a")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateReportSaltA] failed when commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) CreateReportSaltB(input SaltBInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateReportSaltB] failed when init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		createSaltB,
		input.DeviceID,
		input.Iodium,
		input.SampleName,
		input.Lat,
		input.Long,
		input.UserID,
		input.CompanyID,
		input.Counter,
		input.StatusBattery,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateReportSaltB] falied create report salt b")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateRerportSaltB] failed when commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetDetailSaltA(ctx context.Context, saltAID int64) (result SaltAResponse, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getDetailSaltA, whereBuilder)
	err = core.DBConns["main"].Get(
		&result,
		query,
		saltAID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetDetailSaltA] failed get data salt by id")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetDetailSaltB(ctx context.Context, saltBID int64) (result SaltBResponse, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getDetailSaltB, whereBuilder)
	err = core.DBConns["main"].Get(
		&result,
		query,
		saltBID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetDetailSaltB] failed get data salt by id")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetListSaltA(ctx context.Context, startSaltAID int64, maxPerPage int) (result []SaltAResponse, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getListSaltA, whereBuilder)
	err = core.DBConns["main"].Select(
		&result,
		query,
		startSaltAID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListSaltA] failed get data salt a")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetListSaltAWithParameter(ctx context.Context, startSaltAID int64, maxPerPage int, option OptionFilterSaltListQuery) (result []SaltAResponse, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string = "%v"
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("AND company_id = %v %v", companyID, whereBuilder)
	}
	query := fmt.Sprintf(getListSaltAWithOption, whereBuilder)
	query = fmt.Sprintf(query, buildWhareQuery(option))
	err = core.DBConns["main"].Select(
		&result,
		query,
		startSaltAID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListSaltA] failed get data salt a")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetListSaltB(ctx context.Context, startSaltBID int64, maxPerPage int) (result []SaltBResponse, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getListSaltB, whereBuilder)
	err = core.DBConns["main"].Select(
		&result,
		query,
		startSaltBID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListSaltB] failed get data salt a")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetListSaltBWithParameter(ctx context.Context, startSaltBID int64, maxPerPage int, option OptionFilterSaltListQuery) (result []SaltBResponse, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string = "%v"
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("AND company_id = %v %v", companyID, whereBuilder)
	}
	query := fmt.Sprintf(getListSaltBWithOption, whereBuilder)
	query = fmt.Sprintf(query, buildWhareQuery(option))
	err = core.DBConns["main"].Select(
		&result,
		query,
		startSaltBID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListSaltB] failed get data salt B")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetReportMeasurementSaltAList(measurementID int64, maxPerPage int) (result []MeasurementSaltDetail, err error) {
	err = core.DBConns["main"].Select(
		&result,
		measurementSaltAList,
		measurementID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetReportMeasurementSaltAList] failed when get list measurement salt a")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetReportMeasurementSaltBList(measurementID int64, maxPerPage int) (result []MeasurementSaltDetail, err error) {
	err = core.DBConns["main"].Select(
		&result,
		measurementSaltBList,
		measurementID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetReportMeasurementSaltBList] failed when get list measurement salt b")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetReportMeasurementAllSaltList(measurementID int64, maxPerPage int) (result []MeasurementSaltDetail, err error) {
	err = core.DBConns["main"].Select(
		&result,
		measurementAllSaltList,
		measurementID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetReportMeasurementAllSaltList] failed when get list measurement salt")
		log.Println(err)
		return
	}
	return
}

func (core *CoreSalt) GetDetailReportMeasurementSalt(measurementID int64) (result MeasurementSaltDetail, err error) {
	err = core.DBConns["main"].Select(
		&result,
		measurementSaltDetail,
		measurementID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetDetailReportMeasurementAllSalt] failed when get detail measurement salt")
		log.Println(err)
		return
	}
	return
}

func buildWhareQuery(option OptionFilterSaltListQuery) (whareQuery string) {
	if option.UserID != 0 {
		whareQuery = fmt.Sprintf(`%s AND user_id = %v`, whareQuery, option.UserID)
	}
	if option.DeviceID != "" {
		whareQuery = fmt.Sprintf(`%s AND device_id = "%v"`, whareQuery, option.DeviceID)
	}
	return
}
