package salt

import (
	"context"
	"time"

	"github.com/jmoiron/sqlx"
)

type (
	ICoreSalt interface {
		CreateReportSaltA(input SaltAInput) (err error)
		CreateReportSaltB(input SaltBInput) (err error)
		GetDetailSaltA(ctx context.Context, saltAID int64) (result SaltAResponse, err error)
		GetDetailSaltB(ctx context.Context, saltBID int64) (result SaltBResponse, err error)
		GetListSaltA(ctx context.Context, startSaltAID int64, maxPerPage int) (result []SaltAResponse, err error)
		GetListSaltAWithParameter(ctx context.Context, startSaltAID int64, maxPerPage int, option OptionFilterSaltListQuery) (result []SaltAResponse, err error)
		GetListSaltB(ctx context.Context, startSaltBID int64, maxPerPage int) (result []SaltBResponse, err error)
		GetListSaltBWithParameter(ctx context.Context, startSaltBID int64, maxPerPage int, option OptionFilterSaltListQuery) (result []SaltBResponse, err error)
		GetReportMeasurementSaltAList(measurementID int64, maxPerPage int) (result []MeasurementSaltDetail, err error)
		GetReportMeasurementSaltBList(measurementID int64, maxPerPage int) (result []MeasurementSaltDetail, err error)
		GetReportMeasurementAllSaltList(measurementID int64, maxPerPage int) (result []MeasurementSaltDetail, err error)
		GetDetailReportMeasurementSalt(measurementID int64) (result MeasurementSaltDetail, err error)
	}

	CoreSalt struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	SaltAInput struct {
		SaltAID       int64     `db:"salt_a_id" json:"salt_a_id"`
		DeviceID      string    `db:"device_id" json:"device_id"`
		Nacl          float64   `db:"nacl" json:"nacl"`
		Whiteness     float64   `db:"whiteness" json:"whiteness"`
		WaterContent  float64   `db:"water_content" json:"water_content"`
		SampleName    string    `db:"sample_name" json:"sample_name"`
		Lat           float64   `db:"latitude" json:"latitude"`
		Long          float64   `db:"longitude" json:"longitude"`
		UserID        int64     `db:"user_id" json:"user_id"`
		CompanyID     int64     `db:"company_id" json:"company_id"`
		Counter       int64     `db:"counter" json:"counter"`
		StatusBattery int       `db:"status_battery" json:"status_battery"`
		CreateAt      time.Time `db:"create_at" json:"create_at"`
	}

	SaltBInput struct {
		SaltBID       int64     `db:"salt_b_id" json:"salt_b_id"`
		DeviceID      string    `db:"device_id" json:"device_id"`
		Iodium        float64   `db:"iodium" json:"iodium"`
		SampleName    string    `db:"sample_name" json:"sample_name"`
		Lat           float64   `db:"latitude" json:"latitude"`
		Long          float64   `db:"longitude" json:"longitude"`
		UserID        int64     `db:"user_id" json:"user_id"`
		CompanyID     int64     `db:"company_id" json:"company_id"`
		Counter       int64     `db:"counter" json:"counter"`
		StatusBattery int       `db:"status_battery" json:"status_battery"`
		CreateAt      time.Time `db:"create_at" json:"create_at"`
	}

	SaltAResponse struct {
		SaltAID       int64     `db:"salt_a_id" json:"salt_a_id"`
		DeviceID      string    `db:"device_id" json:"device_id"`
		Nacl          float64   `db:"nacl" json:"nacl"`
		Whiteness     float64   `db:"whiteness" json:"whiteness"`
		WaterContent  float64   `db:"water_content" json:"water_content"`
		SampleName    string    `db:"sample_name" json:"sample_name"`
		Lat           float64   `db:"latitude" json:"latitude"`
		Long          float64   `db:"longitude" json:"longitude"`
		UserID        int64     `db:"user_id" json:"user_id"`
		CompanyID     int64     `db:"company_id" json:"company_id"`
		Counter       int64     `db:"counter" json:"counter"`
		StatusBattery int       `db:"status_battery" json:"status_battery"`
		CreateAt      time.Time `db:"create_at" json:"create_at"`
	}

	SaltBResponse struct {
		SaltBID       int64     `db:"salt_b_id" json:"salt_b_id"`
		DeviceID      string    `db:"device_id" json:"device_id"`
		Iodium        float64   `db:"iodium" json:"iodium"`
		SampleName    string    `db:"sample_name" json:"sample_name"`
		Lat           float64   `db:"latitude" json:"latitude"`
		Long          float64   `db:"longitude" json:"longitude"`
		UserID        int64     `db:"user_id" json:"user_id"`
		CompanyID     int64     `db:"company_id" json:"company_id"`
		Counter       int64     `db:"counter" json:"counter"`
		StatusBattery int       `db:"status_battery" json:"status_battery"`
		CreateAt      time.Time `db:"create_at" json:"create_at"`
	}

	SaltAPerPageResponse struct {
		SaltsA      []SaltAResponse `json:"salts_a"`
		LastSaltAID int64           `json:"last_salt_a_id"`
		IsLastPage  bool            `json:"is_last_page"`
	}

	SaltBPerPageResponse struct {
		SaltsB      []SaltBResponse `json:"salts_b"`
		LastSaltBID int64           `json:"last_salt_b_id"`
		IsLastPage  bool            `json:"is_last_page"`
	}

	MeasurementSaltDetail struct {
		MeasurementID int64  `db:"measurement_id" json:"measurement_id"`
		DeviceID      string `db:"device_id" json:"device_id"`
		Counter       int64  `db:"total_measurement" json:"total_measurement"`
		SaltType      int    `db:"salt_type" json:"salt_type"`
	}

	MeasurementSaltList struct {
		MeasurementList   []MeasurementSaltDetail `json:"measurement_list"`
		LastMeasurementID int64                   `json:"last_measurement_id"`
		IsLastPage        bool                    `json:"is_last_page"`
	}

	OptionFilterSaltListQuery struct {
		UserID   int64
		DeviceID string
	}
)
