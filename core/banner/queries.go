package banner

const addBanner = `
INSERT INTO
	banner (
		picture
	)
VALUES
	(?);
`

const editBanner = `
UPDATE
	banner
SET
	picture = ?,
	is_confirmed = ?
WHERE
	banner_id = ?;
`

const deleteBanner = `
UPDATE
	banner
SET
	is_deleted = 1
WHERE
	banner_id = ?;
`

const getSingleBanner = `
SELECT
	banner_id,
	picture,
	is_deleted,
	is_confirmed
FROM
	banner
WHERE
	banner_id = ? AND
	is_deleted = 0;
`

const getListBanner = `
SELECT
	banner_id,
	picture,
	is_deleted,
	is_confirmed
FROM
	banner
WHERE
	banner_id > ? AND
	is_deleted = 0
LIMIT ?;
`

const getBanner = `
SELECT
	banner_id,
	picture,
	is_deleted,
	is_confirmed
FROM
	banner
ORDER BY
	banner_id DESC
LIMIT 3;
`
