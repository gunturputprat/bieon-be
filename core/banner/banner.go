package banner

import (
	"database/sql"
	"github.com/pkg/errors"
	"log"
)

func (core *CoreBanner) AddNewBanner(input BannerInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][AddNewBanner] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		addBanner,
		input.Picutre,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][AddNewBanner] failed insert banner")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][AddNewBanner] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreBanner) EditBanner(input BannerInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][EditBanner] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		editBanner,
		input.Picutre,
		input.IsConfirmed,
		input.BannerID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][EditBanner] failed update banner")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][EditBanner] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreBanner) DeleteBanner(input BannerInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][DeleteBanner] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		deleteBanner,
		input.BannerID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][DeleteBanner] failed delete banner")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][DeleteBanner] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreBanner) GetDetailBanner(bannerID int64) (result BannerInfo, err error) {
	err = core.DBConns["main"].Get(
		&result,
		getSingleBanner,
		bannerID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetDetailBanner] failed get detail banner")
		log.Println(err)
		return
	}
	return
}

func (core *CoreBanner) GetListBanner(startBannerID int64, perPage int) (result []BannerInfo, err error) {
	err = core.DBConns["main"].Select(
		&result,
		getListBanner,
		startBannerID,
		perPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListBanner] failed get banner list")
		log.Println(err)
		return
	}
	return
}

func (core *CoreBanner) GetBanners() (result []BannerInfo, err error) {
	err = core.DBConns["main"].Select(
		&result,
		getBanner,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetBanners] failed get banner")
		log.Println(err)
		return
	}
	return
}
