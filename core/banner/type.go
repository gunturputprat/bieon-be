package banner

import (
	"github.com/jmoiron/sqlx"
)

type (
	ICoreBanner interface {
		AddNewBanner(input BannerInput) (err error)
		EditBanner(input BannerInput) (err error)
		DeleteBanner(input BannerInput) (err error)
		GetDetailBanner(bannerID int64) (result BannerInfo, err error)
		GetListBanner(startBannerID int64, perPage int) (result []BannerInfo, err error)
		GetBanners() (result []BannerInfo, err error)
	}

	CoreBanner struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	BannerInput struct {
		BannerID    int64  `db:"banner_id" json:"banner_id"`
		Picutre     string `db:"picture" json:"picture"`
		IsDeleted   int    `db:"is_deleted" json:"is_deleted"`
		IsConfirmed int    `db:"is_confirmed" json:"is_confirmed"`
	}

	BannerInfo struct {
		BannerID    int64  `db:"banner_id" json:"banner_id"`
		Picutre     string `db:"picture" json:"picture"`
		IsDeleted   int    `db:"is_deleted" json:"is_deleted"`
		IsConfirmed int    `db:"is_confirmed" json:"is_confirmed"`
	}

	BannerList struct {
		Banners      []BannerInfo `json:"banners"`
		LastBannerID int64        `json:"last_banner_id"`
		IsLastPage   bool         `json:"is_last_page"`
	}
)
