package role

import (
	"database/sql"
	"github.com/pkg/errors"
	"log"
)

func (core *CoreRole) AddNewRole(input RoleInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][AddNewRole] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		addNewRole,
		input.RoleName,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][AddNewRole] failed create new role")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][AddNewRole] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreRole) EditRole(input RoleInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][EditRole] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		editRole,
		input.IsDeleted,
		input.RoleID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][EditRole] failed update role")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][EditRole] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreRole) GetRoleList() (result RoleList, err error) {
	err = core.DBConns["main"].Select(
		&result,
		getListRole,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetRoleList] failed get list role")
		log.Println(err)
		return
	}
	return
}

func (core *CoreRole) GetRoleInfoByID(roleID int64) (result RoleInfo, err error) {
	err = core.DBConns["main"].Get(
		&result,
		getRoleInfoByID,
		roleID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetRoleInfoByID] failed get role info")
		log.Println(err)
		return
	}
	return
}
