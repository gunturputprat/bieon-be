package role

import (
	"github.com/jmoiron/sqlx"
)

type (
	ICoreRole interface {
		AddNewRole(input RoleInput) (err error)
		EditRole(input RoleInput) (err error)
		GetRoleList() (result RoleList, err error)
		GetRoleInfoByID(roleID int64) (result RoleInfo, err error)
	}

	CoreRole struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	RoleInput struct {
		RoleID    int64  `db:"role_id" json:"role_id"`
		RoleName  string `db:"role_name" json:"role_name"`
		IsDeleted int    `db:"id_deleted" json:"is_deleted"`
	}

	RoleInfo struct {
		RoleID   int64  `db:"role_id" json:"role_id"`
		RoleName string `db:"role_name" json:"role_name"`
	}

	RoleList struct {
		RoleList []RoleInfo `json:"role_list"`
	}
)
