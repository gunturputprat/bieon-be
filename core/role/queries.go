package role

const addNewRole = `
INSERT INTO
	role(
		role_name
	)
VALUES
	(?);
`

const editRole = `
UPDATE
	role
SET
	is_deleted = ?
WHERE
	role_id = ?;
`

const getListRole = `
SELECT
	role_id,
	role_name,
	is_deleted,
	create_at
FROM
	role;
`

const getRoleInfoByID = `
SELECT
	role_id,
	role_name,
	is_deleted,
	create_at
FROM
	role
WHERE
	role_id = ?;
`
