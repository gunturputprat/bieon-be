package auth

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	"github.com/pkg/errors"
	commonContext "gitlab.com/bieon-be/common/context"
	"gitlab.com/bieon-be/common/state"
)

// CreateUser insert user data into db
func (core *CoreAuth) CreateUser(data UserInput) (userID int64, err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[CreateUser] failed init db tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	result, err := mainTx.Exec(
		createUser,
		data.Email,
		data.Password,
		data.Name,
		data.PhoneNumber,
		data.Role,
		data.Position,
		data.CompanyID,
		data.Address,
		data.Gender,
		data.Picture,
	)
	if err != nil {
		err = errors.Wrap(err, "[CreateUser] failed insert new user")
		log.Println(err)
		return
	}

	userID, err = result.LastInsertId()
	if err != nil {
		err = errors.New("[CreateUser] failed get last user id")
		log.Println(err)
		return
	}

	if userID <= 0 {
		err = errors.New("[CreateUser] unexpected user id")
		log.Println(err)
		return
	}

	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[CreateUser] failed commit tx")
		log.Println(err)
		return
	}

	return
}

func (core *CoreAuth) GetUserInfoByEmail(email string) (userData UserInfo, err error) {
	err = core.DBConns["main"].Get(&userData, selectUserInfoByEmail, email)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[GetUserInfoByEmail] failed get user info")
		log.Println(err)
		return
	}
	return
}

func (core *CoreAuth) GetUserInfoByID(ctx context.Context, userID int64) (userData UserInfo, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	roleID, _ := commonContext.GetRole(ctx)
	var whareBuildQuery string
	if roleID != state.RoleOwner {
		whareBuildQuery = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(selectUserInfoByID, whareBuildQuery)
	err = core.DBConns["main"].Get(&userData, query, userID)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[GetUserInfoByID] failed get user info")
		log.Println(err)
		return
	}
	return
}

func (core *CoreAuth) UpdateUserAccessToken(userID int64, accessToken string) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[UpdateUserAccessToken] init db tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()

	_, err = mainTx.Exec(updateUserAccessToken, accessToken, userID)
	if err != nil {
		err = errors.Wrapf(err, "[UpdateUserAccessToken] failed when update access token")
		log.Println(err)
		return
	}

	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[UpdateUserAccessToken] failed when commit tx")
		log.Println(err)
		return
	}

	return
}

func (core *CoreAuth) UpdateUserData(userData UserInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[UpdateUserData] failed ini db tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()

	_, err = mainTx.Exec(
		updateUserData,
		userData.Name,
		userData.Email,
		userData.Position,
		userData.CompanyID,
		userData.Address,
		userData.PhoneNumber,
		userData.Gender,
		userData.Picture,
		userData.UserID,
	)
	if err != nil {
		err = errors.Wrap(err, "[UpdateUserData] failed when update user info")
		log.Println(err)
		return
	}

	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[UpdateUserData] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreAuth) UpdatePassword(userID int64, password string) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdatePassword] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		updatePassword,
		password,
		userID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdatePassword] failed update password")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdatePassword] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreAuth) DeleteUser(userID int64) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][DeleteUser] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		deleteUser,
		1,
		userID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][DeleteUser] failed delete user")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][DeleteUser] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreAuth) GetListUser(ctx context.Context, lastUserID int64, perPage int) (result []UserInfo, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var buildWhareQuery string
	if userRole != state.RoleOwner {
		buildWhareQuery = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getAllUser, buildWhareQuery)
	err = core.DBConns["main"].Select(
		&result,
		query,
		lastUserID,
		perPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListUser] failed get list user")
		log.Println(err)
		return
	}
	return
}
