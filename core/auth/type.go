package auth

import (
	"context"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/jmoiron/sqlx"
)

type (
	ICoreAuth interface {
		CreateUser(data UserInput) (userID int64, err error)
		GetUserInfoByEmail(email string) (userData UserInfo, err error)
		GetUserInfoByID(ctx context.Context, userID int64) (userData UserInfo, err error)
		UpdateUserAccessToken(userID int64, accessToken string) (err error)
		UpdateUserData(userData UserInput) (err error)
		UpdatePassword(userID int64, password string) (err error)
		DeleteUser(userID int64) (err error)
		GetListUser(ctx context.Context, lastUserID int64, perPage int) (result []UserInfo, err error)
	}

	CoreAuth struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	LoginInput struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	UserInput struct {
		UserID      int64  `db:"user_id" json:"user_id"`
		Email       string `db:"email" json:"email"`
		Password    string `db:"password" json:"password"`
		Name        string `db:"fullname" json:"fullname"`
		PhoneNumber string `db:"phone_number" json:"phone_number"`
		Position    string `db:"position" json:"position"`
		CompanyID   int64  `db:"company_id" json:"company_id"`
		Address     string `db:"address" json:"address"`
		Role        int    `db:"role_user" json:"role_user"`
		Picture     string `db:"picture_user" json:"picture_user"`
		Gender      int    `db:"gender" json:"gender"`
	}

	UserInfo struct {
		UserID      int64  `db:"user_id" json:"user_id"`
		Email       string `db:"email" json:"email"`
		Password    string `db:"password" json:"-"`
		Name        string `db:"fullname" json:"fullname"`
		PhoneNumber string `db:"phone_number" json:"phone_number"`
		Position    string `db:"position" json:"position"`
		CompanyID   int64  `db:"company_id" json:"company_id"`
		Address     string `db:"address" json:"address"`
		Role        int64  `db:"role_user" json:"role_user"`
		Picture     string `db:"picture_user" json:"picture_user"`
		Gender      int    `db:"gender" json:"gender"`
		AccessToken string `db:"access_token" json:"-"`
		IsDeleted   int    `db:"is_deleted" json:"is_deleted"`
	}

	ListUser struct {
		UserInfos  []UserInfo `json:"user_infos"`
		LastUserID int64      `json:"last_user_id"`
		IsLastPage bool       `json:"is_last_page"`
	}

	ChangePasswordInput struct {
		UserID      int64  `json:"user_id"`
		OldPassword string `json:"old_password"`
		NewPassword string `json:"new_password"`
		RePassword  string `json:"re_password"`
		Email       string `json:"email"`
		Token       string `json:"token"`
	}

	TokenInfo struct {
		UserID    int64  `json:"user_id"`
		Email     string `json:"email"`
		RoleID    int64  `json:"role_id"`
		CompanyID int64  `json:"company_id"`
		*jwt.StandardClaims
	}
)
