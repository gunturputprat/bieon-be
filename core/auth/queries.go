package auth

const createUser = `
INSERT INTO
	user(
		email,
		password,
		fullname,
		phone_number,
		role_user,
		position,
		company_id,
		address,
		gender,
		picture_user
	)
VALUES
	(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
`

const selectUserInfoByEmail = `
SELECT
	user_id,
	email,
	password,
	fullname,
	phone_number,
	position,
	company_id,
	address,
	role_user,
	picture_user,
	gender,
	access_token,
	is_deleted
FROM
	user
WHERE
	email = ? AND is_deleted = 0;
`

const selectUserInfoByID = `
SELECT
	user_id,
	email,
	password,
	fullname,
	phone_number,
	position,
	company_id,
	address,
	role_user,
	picture_user,
	gender,
	access_token,
	is_deleted
FROM
	user
WHERE
	user_id = ? AND 
	%v
	is_deleted = 0;
`

const updateUserAccessToken = `
UPDATE
	user
SET
	access_token = ?
WHERE
	user_id = ?;
`

const updateUserData = `
UPDATE
	user
SET
	fullname = ?,
	email = ?,
	position = ?,
	company_id = ?,
	address = ?,
	phone_number = ?,
	gender = ?,
	picture_user = ?
WHERE
	user_id = ?;
`

const updatePassword = `
UPDATE
	user
SET
	password = ?
WHERE
	user_id = ? AND
	is_deleted = 0;
`

const deleteUser = `
UPDATE
	user
SET
	is_deleted = ?
WHERE
	user_id = ?;
`

const getAllUser = `
SELECT
	user_id,
	email,
	password,
	fullname,
	phone_number,
	position,
	role_user,
	picture_user,
	gender,
	company_id,
	access_token,
	is_deleted
FROM
	user
WHERE
	user_id > ? AND
	%v
	is_deleted = 0
LIMIT ?
`
