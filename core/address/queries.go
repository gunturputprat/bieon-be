package address

const getListProvicesData = `
SELECT 
	id as province_id,
	name as province_name
FROM
	provinces;
`

const getListRegencyData = `
SELECT
	id as regency_id,
	province_id,
	name as regency_name
FROM
	regencies
WHERE
	province_id = ?;
`

const getListDistrictData = `
SELECT
	id as district_id,
	name as district_name,
	regency_id
FROM
	districts
WHERE
	regency_id = ?;
`

const getListVillageData = `
SELECT
	id as village_id,
	district_id,
	name as village_name
FROM
	villages
WHERE
	district_id = ?;
`
