package address

import (
	"database/sql"
	"log"

	"github.com/pkg/errors"
)

func (core *CoreAddress) GetListProvicesData() (provinces []Provice, err error) {
	err = core.DBConns["main"].Select(&provinces, getListProvicesData)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[GetListProvicesData] failed to query list province data")
		log.Println(err)
		return
	}

	return
}

func (core *CoreAddress) GetListRegenciesData(proviceID string) (regencies []Regency, err error) {
	if proviceID == "" {
		err = errors.New("[GetListRegenciesData] Invalid province id")
		log.Println(err)
		return
	}

	err = core.DBConns["main"].Select(&regencies, getListRegencyData, proviceID)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrapf(err, "[GetListRegenciesData] failed query regency data by province id -> %s", proviceID)
		log.Println(err)
		return
	}

	return
}

func (core *CoreAddress) GetListDistrictData(regencyID string) (districts []District, err error) {
	if regencyID == "" {
		err = errors.New("[GetListDistrictData] invalid regency ID")
		log.Println(err)
		return
	}

	err = core.DBConns["main"].Select(&districts, getListDistrictData, regencyID)
	if err != nil {
		err = errors.Wrapf(err, "[GetListDistrictData] failed query district data by regency id ->%s", regencyID)
		log.Println(err)
		return
	}

	return
}

func (core *CoreAddress) GetListVillageData(districtID string) (villages []Village, err error) {
	if districtID == "" {
		err = errors.New("[GetListVillageData] Invalid district id")
		log.Println(err)
		return
	}

	err = core.DBConns["main"].Select(&villages, getListVillageData, districtID)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrapf(err, "[GetListVillageData] Failed query village data by district id -> %s", districtID)
		log.Println(err)
		return
	}

	return
}
