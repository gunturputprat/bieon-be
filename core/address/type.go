package address

import (
	"github.com/jmoiron/sqlx"
)

type (
	ICoreAddress interface {
		GetListProvicesData() (provinces []Provice, err error)
		GetListDistrictData(regencyID string) (districts []District, err error)
		GetListRegenciesData(proviceID string) (regencies []Regency, err error)
		GetListVillageData(districtID string) (villages []Village, err error)
	}

	CoreAddress struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	Provice struct {
		ProviceID    string `db:"province_id" json:"province_id"`
		ProvinceName string `db:"province_name" json:"province_name"`
	}

	Regency struct {
		RegencyID   string `db:"regency_id" json:"regency_id"`
		ProviceID   string `db:"province_id" json:"province_id"`
		RegencyName string `db:"regency_name" json:"regency_name"`
	}

	District struct {
		DistrictID   string `db:"district_id" json:"district_id"`
		RegencyID    string `db:"regency_id" json:"regency_id"`
		DistrictName string `db:"district_name" json:"district_name"`
	}

	Village struct {
		VillageID   string `db:"village_id" json:"village_id"`
		DistrictID  string `db:"district_id" json:"district_id"`
		VillageName string `db:"village_name" json:"village_name"`
	}
)
