package company

import (
	"github.com/pkg/errors"
	"log"
)

func (core *CoreCompany) CreateNewCompany(input CompanyInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateNewCompany] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		createNewCompany,
		input.Name,
		input.Address,
		input.PhoneNumber,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateNewCompany] failed create new company")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][CreateNewCompany] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreCompany) UpdateCompany(input CompanyInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateCompany] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		updateCompany,
		input.Name,
		input.Address,
		input.PhoneNumber,
		input.CompanyID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateCompany] failed update company data")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateCompany] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreCompany) UpdateDeleteStatusCompany(companyID int64, isDeleted int) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateDeleteStatusCompany] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		deleteCompany,
		isDeleted,
		companyID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateDeleteStatusCompany] failed update status delete company")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateDeleteStatusCompany] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreCompany) UpdateStatusActiveCompany(companyID int64, isActive int) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateStatusActiveCompany] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		updateActiveStatusCompany,
		isActive,
		companyID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateStatusActiveCompany] failed update status active company")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][UpdateStatusActiveCompany] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreCompany) GetCompanyListPerPage(lastCompanyID int64, maxPerPage int) (result []CompanyInfo, err error) {
	err = core.DBConns["main"].Select(
		&result,
		getAllCompanyListPerPage,
		lastCompanyID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetCompanyListPerPage] falied when get list company")
		log.Println(err)
		return
	}
	return
}

func (core *CoreCompany) GetDetailCompany(companyID int64) (result CompanyInfo, err error) {
	err = core.DBConns["main"].Get(
		&result,
		getDetailCompany,
		companyID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetDetailCompany] failed get company detail data")
		log.Println(err)
		return
	}
	return
}
