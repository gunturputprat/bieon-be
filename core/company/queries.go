package company

const createNewCompany = `
INSERT INTO
	company (
		name,
		address,
		phone_number
	)
VALUES
	(?,?,?);
`

const updateCompany = `
UPDATE
	company
SET
	name = ?,
	address = ?,
	phone_number = ?
WHERE
	company_id = ?;
`

const deleteCompany = `
UPDATE
	company
SET
	is_deleted = ?
WHERE
	company_id = ?;
`

const updateActiveStatusCompany = `
UPDATE
	company
SET
	is_active = ?
WHERE
	company_id = ?;
`

const getAllCompanyListPerPage = `
SELECT
	company_id,
	name,
	address,
	phone_number,
	is_active
FROM
	company
WHERE
	company_id > ? AND
	is_deleted = 0
LIMIT ?;
`

const getDetailCompany = `
SELECT
	company_id,
	name,
	address,
	phone_number,
	is_active
FROM
	company
WHERE
	company_id = ? AND
	is_deleted = 0;
`
