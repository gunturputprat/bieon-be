package company

import (
	"github.com/jmoiron/sqlx"
)

type (
	ICoreCompany interface {
		CreateNewCompany(input CompanyInput) (err error)
		UpdateCompany(input CompanyInput) (err error)
		UpdateDeleteStatusCompany(companyID int64, isDeleted int) (err error)
		UpdateStatusActiveCompany(companyID int64, isActive int) (err error)
		GetCompanyListPerPage(lastCompanyID int64, maxPerPage int) (result []CompanyInfo, err error)
		GetDetailCompany(companyID int64) (result CompanyInfo, err error)
	}

	CoreCompany struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	CompanyInput struct {
		CompanyID   int64  `db:"company_id" json:"company_id"`
		Name        string `db:"name" json:"name"`
		Address     string `db:"address" json:"address"`
		PhoneNumber string `db:"phone_number" json:"phone_number"`
		IsActive    int    `db:"is_active" json:"is_active"`
	}

	CompanyInfo struct {
		CompanyID   int64  `db:"company_id" json:"company_id"`
		Name        string `db:"name" json:"name"`
		Address     string `db:"address" json:"address"`
		PhoneNumber string `db:"phone_number" json:"phone_number"`
		IsActive    int    `db:"is_active" json:"is_active"`
	}

	CompanyListPage struct {
		Companies     []CompanyInfo `json:"companies"`
		LastCompanyID int64         `json:"last_company_id"`
		IsLastPage    bool          `json:"is_last_page"`
	}
)
