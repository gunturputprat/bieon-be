package device

const addDevice = `
INSERT INTO
	device(device_id, company_id, latitude, longitude, status_battery, last_calibration)
VALUES
	(?,?,?,?,?,?);
`

const editDevice = `
UPDATE
	device
SET
	latitude = ?,
	longitude = ?,
	status_battery = ?,
	counter = ?,
	company_id = ?,
	last_calibration = ?
WHERE
	device_id = ? AND
	is_deleted = 0;
`

const getDeviceInfoByDeviceID = `
SELECT
	main_device_id,
	device_id,
	company_id,
	counter,
	latitude,
	longitude,
	COALESCE(last_calibration, now()) AS last_calibration,
	status_battery,
	is_active,
	is_deleted,
	create_at
FROM
	device
WHERE
	device_id = ? AND
	%v
	is_deleted = 0;
`

const getListDeviceInfo = `
SELECT
	main_device_id,
	device_id,
	company_id,
	latitude,
	longitude,
	counter,
	COALESCE(last_calibration, now()) AS last_calibration,
	status_battery,
	is_active,
	is_deleted,
	create_at
FROM
	device
WHERE
	main_device_id < ? AND
	%v
	is_deleted = 0
LIMIT ?;
`

const addMeasurementDevice = `
INSERT INTO
	measurement_device(
		device_id,
		salt_type
	)
VALUES
	(?,?);
`

const addDeviceCounter = `
UPDATE
	device
SET
	counter = ?
WHERE
	device_id = ?;
`

const addTotalMeasurement = `
UPDATE
	measurement_device
SET
	total_measurement = ?
WHERE
	device_id = ? AND
	salt_type = ?;
`
