package device

import (
	"context"
	"github.com/jmoiron/sqlx"
	"time"
)

type (
	ICoreDevice interface {
		AddDevice(input DeviceInput) (err error)
		GetDeviceInfoByID(ctx context.Context, deviceID string) (result DeviceInfo, err error)
		EditDevice(input DeviceInput) (err error)
		GetListDevice(ctx context.Context, mainDeviceID int64, maxPerPage int) (result []DeviceInfo, err error)
	}

	CoreDevice struct {
		DBConns map[string]*sqlx.DB `inject:"db_conns"`
	}

	DeviceInput struct {
		MainDeviceID    int64     `db:"main_device_id" json:"main_device_id"`
		DeviceID        string    `db:"device_id" json:"device_id"`
		CompanyID       int64     `db:"company_id" json:"company_id"`
		Latitude        float64   `db:"latitude" json:"latitude"`
		Longiture       float64   `db:"longitude" json:"longitude"`
		StatusBattery   int       `db:"status_battery" json:"status_battery"`
		Counter         int64     `db:"counter" json:"counter"`
		LastCalibration time.Time `db:"last_calibration" json:"last_calibration"`
		IsActive        int       `db:"is_active" json:"is_active"`
		IsDeleted       int       `db:"is_deleted" json:"is_deleted"`
	}

	DeviceInfo struct {
		MainDeviceID    int64     `db:"main_device_id" json:"main_device_id"`
		DeviceID        string    `db:"device_id" json:"device_id"`
		CompanyID       int64     `db:"company_id" json:"company_id"`
		Counter         int64     `db:"counter" json:"counter"`
		Latitude        float64   `db:"latitude" json:"latitude"`
		Longiture       float64   `db:"longitude" json:"longitude"`
		StatusBattery   int       `db:"status_battery" json:"status_battery"`
		LastCalibration time.Time `db:"last_calibration" json:"last_calibration"`
		IsActive        int       `db:"is_active" json:"is_active"`
		IsDeleted       int       `db:"is_deleted" json:"is_deleted"`
		CreateAt        time.Time `db:"create_at" json:"create_at"`
	}

	ListDeviceInfo struct {
		Devices      []DeviceInfo `json:"devices"`
		LastDeviceID int64        `json:"last_device_id"`
		IsLastPage   bool         `json:"is_last_page"`
	}
)
