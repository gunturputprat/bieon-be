package device

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/bieon-be/common/state"
	"log"

	commonContext "gitlab.com/bieon-be/common/context"
)

func (core *CoreDevice) AddDevice(input DeviceInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][AddDevice] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		addDevice,
		input.DeviceID,
		input.CompanyID,
		input.Latitude,
		input.Longiture,
		input.StatusBattery,
		input.LastCalibration,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][AddDevice] failed insert new device")
		log.Println(err)
		return
	}
	_, err = mainTx.Exec(
		addMeasurementDevice,
		input.DeviceID,
		1,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][AddDevice] failed insert measurement_device for salt a")
		log.Println(err)
		return
	}
	_, err = mainTx.Exec(
		addMeasurementDevice,
		input.DeviceID,
		2,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][AddDevice] failed insert measurement_device for salt b")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][AddDevice] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreDevice) GetDeviceInfoByID(ctx context.Context, deviceID string) (result DeviceInfo, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBuilder string
	if userRole != state.RoleOwner {
		whereBuilder = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getDeviceInfoByDeviceID, whereBuilder)
	err = core.DBConns["main"].Get(
		&result,
		query,
		deviceID,
	)
	if err != nil && err != sql.ErrNoRows {
		err = errors.Wrap(err, "[Core][GetDeviceInfoByID] failed when select query")
		log.Println(err)
		return
	}
	return
}

func (core *CoreDevice) EditDevice(input DeviceInput) (err error) {
	mainTx, err := core.DBConns["main"].Beginx()
	if err != nil {
		err = errors.Wrap(err, "[Core][EditDevice] failed init tx")
		log.Println(err)
		return
	}
	defer mainTx.Rollback()
	_, err = mainTx.Exec(
		editDevice,
		input.Latitude,
		input.Longiture,
		input.StatusBattery,
		input.Counter,
		input.CompanyID,
		input.LastCalibration,
		input.DeviceID,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][EditDevice] failed update device data")
		log.Println(err)
		return
	}
	err = mainTx.Commit()
	if err != nil {
		err = errors.Wrap(err, "[Core][EditDevice] failed commit tx")
		log.Println(err)
		return
	}
	return
}

func (core *CoreDevice) GetListDevice(ctx context.Context, mainDeviceID int64, maxPerPage int) (result []DeviceInfo, err error) {
	companyID, _ := commonContext.GetCompany(ctx)
	userRole, _ := commonContext.GetRole(ctx)
	var whereBulder string
	if userRole != state.RoleOwner {
		whereBulder = fmt.Sprintf("company_id = %v AND", companyID)
	}
	query := fmt.Sprintf(getListDeviceInfo, whereBulder)
	err = core.DBConns["main"].Select(
		&result,
		query,
		mainDeviceID,
		maxPerPage,
	)
	if err != nil {
		err = errors.Wrap(err, "[Core][GetListDevice] failed get data list device")
		log.Println(err)
		return
	}
	return
}
